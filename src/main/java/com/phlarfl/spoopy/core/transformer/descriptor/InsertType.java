package com.phlarfl.spoopy.core.transformer.descriptor;

public enum InsertType {

    BEFORE,
    AFTER,
    AT

}
