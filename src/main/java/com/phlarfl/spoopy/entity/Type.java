package com.phlarfl.spoopy.entity;

public interface Type<T> {

    Class<? extends T>[] getClasses();

}
