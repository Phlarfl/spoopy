package com.phlarfl.spoopy.attribute.type;

import com.phlarfl.spoopy.attribute.exception.AttributeException;
import com.phlarfl.spoopy.attribute.exception.InvalidAttributeValueException;
import com.phlarfl.spoopy.util.ClassHelper;
import com.phlarfl.spoopy.util.callback.TriFunction;

import java.lang.reflect.Field;
import java.util.function.Function;

public enum AttributeType {
    BYTE(Byte.TYPE, Byte::parseByte),
    BOOLEAN(Boolean.TYPE, Boolean::parseBoolean),
    CHAR(Character.TYPE, (value) -> value.charAt(0)),
    SHORT(Short.TYPE, Short::parseShort),
    INT(Integer.TYPE, Integer::parseInt),
    LONG(Long.TYPE, Long::parseLong),
    FLOAT(Float.TYPE, Float::parseFloat),
    DOUBLE(Double.TYPE, Double::parseDouble),
    STRING(String.class, (value) -> value),
    ENUM(Enum.class, (field, object, value) -> Enum.valueOf((Class<Enum>) field.getType(), value.toUpperCase())),
    CUSTOM_ATTRIBUTE(AbstractAttributeType.class, (field, object, value) -> {
        return object.parse(value);
    });

    private Class<?> type;
    private TriFunction<Field, ?, String, ?> function;

    <T> AttributeType(Class<T> type, Function<String, T> function) {
        this(type, (field, object, value) -> function.apply(value));
    }

    <T> AttributeType(Class<T> type, TriFunction<Field, T, String, T> function) {
        this.type = type;
        this.function = function;
    }

    public Class<?> getType() {
        return type;
    }

    public <T> T getValue(Object object, Field field) {
        return ClassHelper.getFieldValue(object, field);
    }

    public void setValue(Object object, Field field, String value) throws AttributeException {
        Object newValue = function.accept(field, ClassHelper.getFieldValue(object, field), value);
        if (newValue == null)
            throw new InvalidAttributeValueException();
        ClassHelper.setFieldValue(object, field, newValue);
    }

    public static AttributeType getType(Class<?> cls) {
        for (AttributeType type : values())
            if (type.getType() != null && ClassHelper.classOf(cls, type.getType()))
                return type;
        return null;
    }
}
