package com.phlarfl.spoopy.util.callback;

public interface Callback {

    void call();

}
