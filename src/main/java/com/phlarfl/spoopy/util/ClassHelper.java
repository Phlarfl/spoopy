package com.phlarfl.spoopy.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ClassHelper {

    public static String getPackageName(Package pkg) {
        return pkg.getName();
    }

    public static String getPackageName(Class cls) {
        return getPackageName(cls.getPackage());
    }

    public static String getPackageName(Object obj) {
        return getPackageName(obj.getClass());
    }

    public static boolean classOf(Class cls, Class of) {
        return cls == of || (cls != null && cls != Object.class && (classOf(cls.getSuperclass(), of) || classOf(cls.getInterfaces(), of)));
    }

    public static boolean classOf(Class[] classes, Class of) {
        for (Class cls : classes)
            if (classOf(cls, of))
                return true;
        return false;
    }

    public static Field[] getFieldsByType(Field[] fields, Class<?> type) {
        return Arrays.stream(fields).filter((field) -> field.getType() == type).toArray(Field[]::new);
    }

    public static boolean setFieldValue(Object object, Field field, Object value) {
        try {
            field.set(object, value);
            return true;
        } catch (IllegalAccessException e) {
        }
        return false;
    }

    public static <T> T getFieldValue(Object object, Field field) {
        try {
            return (T) field.get(object);
        } catch (IllegalAccessException e) {
        }
        return null;
    }

    public static boolean isAbstractOrInterface(Class<?> cls) {
        return Modifier.isAbstract(cls.getModifiers()) || Modifier.isInterface(cls.getModifiers());
    }

}
