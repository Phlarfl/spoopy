package com.phlarfl.spoopy.util;

public class OpsHelper {

    public static <T> T ternary(boolean condition, T t, T f) {
        return condition ? t : f;
    }

    public static <T> T orElse(T value, T matcher, T orElse) {
        return value == matcher ? value : orElse;
    }

    public static <T> T elseOr(T value, T matcher, T elseOr) {
        return value == matcher ? elseOr : value;
    }

}
