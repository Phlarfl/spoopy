package com.phlarfl.spoopy.util;

import com.phlarfl.spoopy.render.Glu;
import net.minecraft.util.math.Vec3d;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;

public class RenderHelper implements MinecraftHelper {

    private static final FloatBuffer outputBuffer = BufferUtils.createFloatBuffer(3);
    private static final FloatBuffer modelBuffer = BufferUtils.createFloatBuffer(16);
    private static final FloatBuffer projectionBuffer = BufferUtils.createFloatBuffer(16);
    private static final IntBuffer viewportBuffer = BufferUtils.createIntBuffer(16);

    private static void setBuffers() {
        outputBuffer.clear();
        modelBuffer.clear();
        projectionBuffer.clear();
        viewportBuffer.clear();

        glGetFloatv(GL_MODELVIEW_MATRIX, modelBuffer);
        glGetFloatv(GL_PROJECTION_MATRIX, projectionBuffer);
        glGetIntegerv(GL_VIEWPORT, viewportBuffer);
    }

    public static Vec3d worldToScreen(Vec3d pos) {
        pos = pos.subtract(EntityHelper.getCameraPosition());
        setBuffers();
        return Glu.project(pos, modelBuffer, projectionBuffer, viewportBuffer, outputBuffer) ? new Vec3d(outputBuffer.get(0), mc.mainWindow.getHeight() - outputBuffer.get(1), outputBuffer.get(2)) : null;
    }

    public static Vec3d screenToWorld(float x, float y, float z) {
        setBuffers();
        return Glu.unProject(x, y, z, modelBuffer, projectionBuffer, viewportBuffer, outputBuffer) ? new Vec3d(outputBuffer.get(0), outputBuffer.get(1), outputBuffer.get(2)) : null;
    }

}
