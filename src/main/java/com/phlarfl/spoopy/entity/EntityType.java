package com.phlarfl.spoopy.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.boss.WitherEntity;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.item.*;
import net.minecraft.entity.item.minecart.AbstractMinecartEntity;
import net.minecraft.entity.merchant.villager.AbstractVillagerEntity;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.passive.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.*;

public enum EntityType implements Type<Entity> {
    BOSS(new Class[]{
            EnderDragonEntity.class,
            WitherEntity.class
    }),
    ITEM(new Class[]{
            AbstractMinecartEntity.class,
            ArmorStandEntity.class,
            BoatEntity.class,
            EnderCrystalEntity.class,
            EnderPearlEntity.class,
            ExperienceBottleEntity.class,
            ExperienceOrbEntity.class,
            EyeOfEnderEntity.class,
            FallingBlockEntity.class,
            FireworkRocketEntity.class,
            HangingEntity.class,
            ItemEntity.class,
            TNTEntity.class
    }),
    MERCHANT(new Class[]{
            AbstractVillagerEntity.class
    }),
    MONSTER(new Class[]{
            GhastEntity.class,
            MonsterEntity.class,
            PhantomEntity.class,
            ShulkerEntity.class,
            SlimeEntity.class
    }),
    PASSIVE(new Class[]{
            AmbientEntity.class,
            AnimalEntity.class,
            IronGolemEntity.class,
            SnowGolemEntity.class,
            WaterMobEntity.class,
    }),
    PLAYER(new Class[]{
            PlayerEntity.class
    }),
    PROJECTILE(new Class[]{
            AbstractArrowEntity.class,
            DamagingProjectileEntity.class,
            EvokerFangsEntity.class,
            FishingBobberEntity.class,
            LlamaSpitEntity.class,
            ShulkerBulletEntity.class,
            ThrowableEntity.class,
    }),
    UNKNOWN(new Class[]{});

    private Class<? extends Entity>[] classes;

    EntityType(Class<? extends Entity>[] classes) {
        this.classes = classes;
    }

    @Override
    public Class<? extends Entity>[] getClasses() {
        return classes;
    }
}
