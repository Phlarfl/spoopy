package com.phlarfl.spoopy.util.request;

import com.phlarfl.spoopy.util.LogHelper;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class Request {

    public enum Method {
        GET(HttpGet.class),
        POST(HttpPost.class),
        HEAD(HttpHead.class),
        PUT(HttpPut.class),
        DELETE(HttpDelete.class);

        private Class<? extends HttpRequestBase> typeClass;

        Method(Class<? extends HttpRequestBase> typeClass) {
            this.typeClass = typeClass;
        }

        public Class<? extends HttpRequestBase> getTypeClass() {
            return typeClass;
        }
    }

    private Method method;
    private String url;
    private List<Header> headers = new ArrayList<>();

    public Request(Method method, String url) {
        this.method = method;
        this.url = url;
    }

    public Request setHeader(String key, String value) {
        headers.add(new BasicHeader(key, value));
        return this;
    }

    public Request setHeaders(String... keyValues) {
        for (int i = 0; i < keyValues.length; i += 2)
            if (i < keyValues.length - 1)
                setHeader(keyValues[i], keyValues[i + 1]);
        return this;
    }

    public String execute() {
        HttpClient client = HttpClients.createMinimal();
        HttpRequestBase request;
        try {
            request = method.getTypeClass().getConstructor(String.class).newInstance(url);
            request.setHeaders(headers.toArray(new Header[0]));
            return client.execute(request, (response) -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                }
                return null;
            });
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | IOException e) {
            LogHelper.error(new IOException(String.format("could not create new request of type %s", method.getTypeClass())));
            return null;
        }
    }

}
