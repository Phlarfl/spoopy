package com.phlarfl.spoopy.render.texture;

import net.minecraft.client.renderer.texture.NativeImage;
import net.minecraft.util.ResourceLocation;
import org.apache.http.HttpHeaders;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Texture {

    private String url;
    private ResourceLocation resource;
    private NativeImage image;
    private int width, height;

    public Texture(String url) {
        this.url = url;
    }

    public ResourceLocation getResource() {
        return resource;
    }

    public void setResource(ResourceLocation resource) {
        this.resource = resource;
    }

    public NativeImage getImage() {
        return image;
    }

    public void setImage(NativeImage image) {
        this.image = image;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isDownloaded() {
        return image != null && resource == null;
    }

    public void download() {
        new Thread(() -> {
            try {
                HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
                con.setRequestProperty(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13");
                if (con.getResponseCode() == 200)
                    setImage(NativeImage.read(con.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
