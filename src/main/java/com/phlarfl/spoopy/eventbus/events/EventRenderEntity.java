package com.phlarfl.spoopy.eventbus.events;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.Entity;
import net.minecraftforge.eventbus.api.Event;

@Getter
@Setter
public class EventRenderEntity extends Event {

    private final EntityRendererManager entityRendererManager;
    private final EntityRenderer<Entity> entityRenderer;
    private final Entity entity;
    private final double x, y, z;
    private final float yaw, partialTicks;

    public EventRenderEntity(EntityRendererManager entityRendererManager, Entity entity, double x, double y, double z, float yaw, float partialTicks) {
        this.entityRendererManager = entityRendererManager;
        entityRenderer = entityRendererManager.getRenderer(entity);
        this.entity = entity;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.partialTicks = partialTicks;
    }
}
