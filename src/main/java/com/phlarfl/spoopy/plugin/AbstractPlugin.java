package com.phlarfl.spoopy.plugin;

import com.phlarfl.spoopy.attribute.AbstractAttributeFactory;
import com.phlarfl.spoopy.attribute.Attribute;
import com.phlarfl.spoopy.attribute.AttributeDescriptor;
import com.phlarfl.spoopy.attribute.InvalidAttributeType;
import com.phlarfl.spoopy.attribute.exception.AttributeException;
import com.phlarfl.spoopy.command.Command;
import com.phlarfl.spoopy.command.CommandFactory;
import com.phlarfl.spoopy.command.exception.CommandException;
import com.phlarfl.spoopy.util.ChatHelper;
import com.phlarfl.spoopy.util.LocaleHelper;
import com.phlarfl.spoopy.util.LogHelper;
import joptsimple.internal.Strings;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.lwjgl.glfw.GLFW;

import java.util.Arrays;

public abstract class AbstractPlugin extends AbstractAttributeFactory implements Command {

    public enum BindMode {
        TOGGLE,
        HOLD
    }

    private transient String id;

    @AttributeDescriptor(id = "name", property = "plugins.attribute.name")
    public String name;

    @AttributeDescriptor(id = "description", property = "plugins.attribute.description")
    public String description;

    @AttributeDescriptor(id = "enabled", property = "plugins.attribute.enabled")
    public boolean enabled = false;

    @AttributeDescriptor(id = "bind", property = "plugins.attribute.bind")
    public int bind = 0;

    @AttributeDescriptor(parentId = "bind", id = "mode", property = "plugins.attribute.bind_mode")
    public BindMode bindMode = BindMode.TOGGLE;

    public AbstractPlugin() {
        PluginDescriptor descriptor = getClass().getAnnotation(PluginDescriptor.class);
        id = descriptor.id();
        name = LocaleHelper.format(String.format("plugin.%s.name", id));
        description = LocaleHelper.format(String.format("plugin.%s.description", id));

        MinecraftForge.EVENT_BUS.register(this);

        setupAttributeFactory();
        CommandFactory.INSTANCE.register(this);
    }

    @Override
    public String getCommandId() {
        return id;
    }

    @Override
    public String getCommandName() {
        return name;
    }

    @Override
    public String getCommandDescription() {
        return description;
    }

    @Override
    public String getCommandAlias() {
        return LocaleHelper.format(String.format("plugin.%s.alias", getCommandId()));
    }

    @Override
    public String getCommandUsage() {
        return LocaleHelper.format("plugins.command.usage", getCommandId());
    }

    @Override
    public int getCommandMinArgs() {
        return -1;
    }

    @Override
    public int getCommandMaxArgs() {
        return -1;
    }

    protected void onEnable() {
        enabled = true;
        ChatHelper.print("plugins.on.enable", name);
    }

    protected void onDisable() {
        enabled = false;
        ChatHelper.print("plugins.on.disable", name);
    }

    @SubscribeEvent
    public void onKeyInputEvent(InputEvent.KeyInputEvent event) {
        if (event.getKey() == bind) {
            switch (bindMode) {
                case TOGGLE:
                    if (event.getAction() == GLFW.GLFW_PRESS)
                        if (enabled) onDisable();
                        else onEnable();
                    break;
                case HOLD:
                    if (event.getAction() == GLFW.GLFW_PRESS && !enabled) onEnable();
                    else if (event.getAction() == GLFW.GLFW_RELEASE && enabled) onDisable();
                    break;
            }
        }
    }

    @Override
    public void executeCommand(String[] args) throws CommandException {
        switch (args.length) {
            case 0:
                if (enabled) onDisable();
                else onEnable();
                break;
            default:
                StringBuilder search = new StringBuilder();
                Attribute attribute = null;
                int i;
                for (i = 0; i < args.length; i++) {
                    String s = args[i];
                    if (!search.toString().isEmpty()) search.append(".");
                    search.append(s);
                    Attribute tempAttribute = getAttribute(search.toString());
                    if (tempAttribute == null)
                        break;
                    attribute = tempAttribute;
                }
                if (attribute != null) {
                    String value = Strings.join(Arrays.copyOfRange(args, i, args.length), " ");
                    try {
                        if (!value.isEmpty()) {
                            attribute.setValue(value);
                            ChatHelper.print("plugins.command.attribute.value.set", attribute.getName(), attribute.getValue());
                        } else
                            ChatHelper.print("plugins.command.attribute.value", attribute.getName(), attribute.getValue());
                    } catch (IllegalAccessException | IllegalArgumentException | InvalidAttributeType | AttributeException e) {
                        ChatHelper.print("plugins.command.attribute.error.set", attribute.getName(), value);
                        LogHelper.error(e);
                    }
                } else ChatHelper.print("plugins.command.attribute.error.invalid", search.toString(), name);
                break;
        }
    }

}
