package com.phlarfl.spoopy.render.fbo;

import java.util.HashMap;
import java.util.Map;

public enum FboFactory {
    INSTANCE;

    private final Map<String, Fbo> FBO_MAP = new HashMap<>();
    private boolean bound;

    public Fbo register(Class container, String name, int scale, boolean depth) {
        Fbo fbo = new Fbo(scale, depth);
        FBO_MAP.put(String.format("%s_%s", container.getSimpleName(), name), fbo);
        return fbo;
    }

    public Fbo getFbo(String name) {
        return FBO_MAP.get(name);
    }

    public Fbo getFbo(Class container, String name, int scale) {
        return FBO_MAP.getOrDefault(name, register(container, name, scale, true));
    }

    public void bind(boolean bound) {
        this.bound = bound;
    }

    public boolean isBound() {
        return bound;
    }


}
