package com.phlarfl.spoopy.render.fbo;

import com.mojang.blaze3d.platform.GLX;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.shader.Framebuffer;
import org.lwjgl.opengl.GL11;

public class HFramebuffer extends Framebuffer {

    private int alpha = 255;

    public HFramebuffer(int width, int height, boolean useDepth, boolean onMac) {
        super(width, height, useDepth, onMac);
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    @Override
    public void framebufferRenderExt(int width, int height, boolean disableBlend) {
        if (GLX.isUsingFBOs()) {
//            GlStateManager.colorMask(true, true, true, false);
//            GlStateManager.disableDepthTest();
//            GlStateManager.depthMask(false);
//            GlStateManager.matrixMode(5889);
//            GlStateManager.loadIdentity();
//            GlStateManager.ortho(0.0D, (double)width, (double)height, 0.0D, 1000.0D, 3000.0D);
//            GlStateManager.matrixMode(5888);
//            GlStateManager.loadIdentity();
//            GlStateManager.translatef(0.0F, 0.0F, -2000.0F);
//            GlStateManager.viewport(0, 0, width, height);
            boolean enableTexture = GL11.glIsEnabled(GL11.GL_TEXTURE_2D);
            GlStateManager.enableTexture();
//            GlStateManager.disableLighting();
            boolean alphaTest = GL11.glIsEnabled(GL11.GL_ALPHA_TEST);
            GlStateManager.disableAlphaTest();

            boolean blend = GL11.glIsEnabled(GL11.GL_BLEND);
            boolean colorMaterial = GL11.glIsEnabled(GL11.GL_COLOR_MATERIAL);
            if (disableBlend) {
                GlStateManager.disableBlend();
                GlStateManager.enableColorMaterial();
            } else {
                GlStateManager.enableBlend();
            }

//            GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.bindFramebufferTexture();
            float w = (float) width;
            float h = (float) height;
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder bufferbuilder = tessellator.getBuffer();
            bufferbuilder.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR);
            bufferbuilder.pos(0, h, 0).tex(0, 0).color(255, 255, 255, getAlpha()).endVertex();
            bufferbuilder.pos(w, h, 0).tex(1, 0).color(255, 255, 255, getAlpha()).endVertex();
            bufferbuilder.pos(w, 0, 0).tex(1, 1).color(255, 255, 255, getAlpha()).endVertex();
            bufferbuilder.pos(0, 0, 0).tex(0, 1).color(255, 255, 255, getAlpha()).endVertex();
            tessellator.draw();
            this.unbindFramebufferTexture();

            if (!enableTexture) GlStateManager.disableTexture();
            if (alphaTest) GlStateManager.enableAlphaTest();
            if (blend) GlStateManager.enableBlend();
            else GlStateManager.disableBlend();
            if (colorMaterial) GlStateManager.enableColorMaterial();
            else GlStateManager.disableColorMaterial();
//            GlStateManager.depthMask(true);
//            GlStateManager.colorMask(true, true, true, true);
        }
    }

}
