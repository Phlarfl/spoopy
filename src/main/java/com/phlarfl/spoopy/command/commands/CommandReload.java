package com.phlarfl.spoopy.command.commands;

import com.phlarfl.spoopy.command.Command;
import com.phlarfl.spoopy.command.exception.CommandException;
import com.phlarfl.spoopy.command.exception.InvalidArgumentException;
import com.phlarfl.spoopy.render.shader.AbstractShader;
import com.phlarfl.spoopy.render.shader.ShaderFactory;
import com.phlarfl.spoopy.util.request.Request;

public class CommandReload implements Command {

    @Override
    public String getCommandId() {
        return "reload";
    }

    @Override
    public int getCommandMinArgs() {
        return 1;
    }

    @Override
    public int getCommandMaxArgs() {
        return 2;
    }

    @Override
    public void executeCommand(String[] args) throws CommandException {
        if (args.length == 3) {
            AbstractShader shader = ShaderFactory.INSTANCE.getShader(args[0]);
            if (shader == null)
                throw new InvalidArgumentException(args[0]);
            shader.reload(() -> new Request(Request.Method.GET, args[1]).execute(),
                    () -> new Request(Request.Method.GET, args[2]).execute());
        } else {
            ShaderFactory.INSTANCE.reloadShaders();
        }
    }

}
