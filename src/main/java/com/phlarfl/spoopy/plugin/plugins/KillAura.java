package com.phlarfl.spoopy.plugin.plugins;

import com.mojang.blaze3d.platform.GlStateManager;
import com.phlarfl.spoopy.attribute.AttributeDescriptor;
import com.phlarfl.spoopy.attribute.type.ColorAttribute;
import com.phlarfl.spoopy.entity.EntityFactory;
import com.phlarfl.spoopy.eventbus.EventFactory;
import com.phlarfl.spoopy.eventbus.events.EventRenderEntity;
import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import com.phlarfl.spoopy.render.fbo.Fbo;
import com.phlarfl.spoopy.render.fbo.FboFactory;
import com.phlarfl.spoopy.render.shader.ShaderFactory;
import com.phlarfl.spoopy.render.shader.shaders.ColorShader;
import com.phlarfl.spoopy.render.shader.shaders.OutlineShader;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@PluginDescriptor(id = "killaura")
public class KillAura extends AbstractPlugin {

    //@formatter:off

    @AttributeDescriptor(id = "glow", property = "plugin.killaura.attribute.glow")
    public boolean glow = true;

        @AttributeDescriptor(parentId = "glow", id = "outline", property = "plugin.killaura.attribute.glow.outline")
        public boolean glowOutline = true;

    @AttributeDescriptor(id = "width", property = "plugin.killaura.attribute.width")
    public int width = 3;

    @AttributeDescriptor(id = "range", property = "plugin.killaura.attribute.range")
    public double range = 5;

    @AttributeDescriptor(id = "targetColor", property = "plugin.killaura.attribute.targetColor")
    public ColorAttribute targetColor = new ColorAttribute(0xFFFF0000);

    @AttributeDescriptor(id = "entities", property = "plugin.killaura.attribute.entities")
    public Void entities;

        @AttributeDescriptor(parentId = "entities", id = "bosses", property = "plugin.killaura.attribute.entities.bosses")
        public boolean entitiesBosses = true;

        @AttributeDescriptor(parentId = "entities", id = "merchants", property = "plugin.killaura.attribute.entities.merchants")
        public boolean entitiesMerchants = true;

        @AttributeDescriptor(parentId = "entities", id = "monsters", property = "plugin.killaura.attribute.entities.monsters")
        public boolean entitiesMonsters = true;

        @AttributeDescriptor(parentId = "entities", id = "passives", property = "plugin.killaura.attribute.entities.passives")
        public boolean entitiesPassives = true;

        @AttributeDescriptor(parentId = "entities", id = "players", property = "plugin.killaura.attribute.entities.players")
        public boolean entitiesPlayers = true;

    //@formatter:on

    private transient Fbo modelBuffer, outlineShaderBuffer;

    private transient ColorShader colorShader;
    private transient OutlineShader outlineShader;

    private transient Entity target;
    private transient long lastAttack = 0;
    private transient long attackInterval = 1000;

    private transient boolean invisibles = true;
    private transient boolean invincibles = true;
    private transient boolean spectators = true;

    public KillAura() {
        super();

        modelBuffer = FboFactory.INSTANCE.register(getClass(), "model_buffer", 1, true);
        outlineShaderBuffer = FboFactory.INSTANCE.register(getClass(), "outline_shader_buffer", 1, true);

        colorShader = ShaderFactory.INSTANCE.getShader(ColorShader.class);
        outlineShader = ShaderFactory.INSTANCE.getShader(OutlineShader.class);

        EventFactory.INSTANCE.register(EventRenderEntity.class, 0, this::onRenderEntityEvent);
    }

    private void onRenderEntityEvent(EventRenderEntity event) {
        if (enabled && isSelectable(event.getEntity()) && event.getEntity().equals(target)) {
            enabled = false;
            modelBuffer.bind();
            colorShader.bind(targetColor.getColor());
            event.getEntityRendererManager().setRenderOutlines(true);
            event.getEntityRenderer().doRender(event.getEntity(), event.getX(), event.getY(), event.getZ(), 0, mc.getRenderPartialTicks());
            event.getEntityRendererManager().setRenderOutlines(false);
            colorShader.unbind();
            modelBuffer.unbind();
            enabled = true;
        }
    }

    @SubscribeEvent
    public void onRenderGameOverlayEvent(RenderGameOverlayEvent.Pre event) {
        if (enabled && event.getType() == RenderGameOverlayEvent.ElementType.ALL && target != null) {
            GlStateManager.pushMatrix();
            double scale = mc.mainWindow.getGuiScaleFactor() / Math.pow(mc.mainWindow.getGuiScaleFactor(), 2);
            GlStateManager.scaled(scale, scale, scale);

            outlineShaderBuffer.setColor(0, 0, 0, 0);
            outlineShaderBuffer.bind();

            if (outlineShader != null) outlineShader.bind(width, glow, (float) Math.pow(width, 2), glowOutline);
            modelBuffer.getFramebuffer().framebufferRenderExt(modelBuffer.getWidth(), modelBuffer.getHeight(), true);
            if (outlineShader != null) outlineShader.unbind();

            outlineShaderBuffer.unbind();

            outlineShaderBuffer.getFramebuffer().framebufferRenderExt(outlineShaderBuffer.getWidth(), outlineShaderBuffer.getHeight(), false);

            modelBuffer.setColor(0, 0, 0, 0);
            modelBuffer.unbind();

            GlStateManager.popMatrix();
        }
    }

    @SubscribeEvent
    public void onClientTickEvent(TickEvent.ClientTickEvent event) {
        if (enabled && mc.world != null && mc.player != null) {
            Vec3d playerVec = mc.player.getEyePosition(mc.getRenderPartialTicks());
            target = null;
            for (Entity entity : mc.world.getAllEntities())
                if (isTargetable(playerVec, entity))
                    target = entity;
            if (target != null) {
                if (lastAttack < System.currentTimeMillis() - attackInterval) {
                    lastAttack = System.currentTimeMillis();
                    mc.playerController.attackEntity(mc.player, target);
                }
            } else lastAttack = 0;
        }
    }

    private boolean isSelectable(Entity entity) {
        switch (EntityFactory.INSTANCE.getType(entity)) {
            case BOSS:
                return entitiesBosses;
            case MERCHANT:
                return entitiesMerchants;
            case MONSTER:
                return entitiesMonsters;
            case PASSIVE:
                return entitiesPassives;
            case PLAYER:
                return entitiesPlayers;
            case UNKNOWN:
            default:
                return false;
        }
    }

    private boolean isTargetable(Vec3d playerVec, Entity entity) {
        return entity != mc.player
                && entity instanceof LivingEntity
                && isSelectable(entity)
                && entity.isAlive()
                && (!entity.isInvisible() || invisibles)
                && (!entity.isInvulnerable() || invincibles)
                && (!entity.isSpectator() || spectators)
                && playerVec.squareDistanceTo(getCenter(entity)) < range;
    }

    private Vec3d getCenter(Entity entity) {
        return entity.getPositionVec().add(0, entity.getHeight() / 2., 0);
    }

}
