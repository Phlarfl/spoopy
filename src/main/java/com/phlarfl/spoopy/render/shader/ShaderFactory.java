package com.phlarfl.spoopy.render.shader;

import com.phlarfl.spoopy.util.ClassHelper;
import com.phlarfl.spoopy.util.LogHelper;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public enum ShaderFactory {
    INSTANCE;

    private final Map<Class, AbstractShader> SHADER_MAP = new HashMap<>();

    public void register() {
        new Reflections(ClassHelper.getPackageName(AbstractShader.class))
                .getSubTypesOf(AbstractShader.class)
                .forEach((cls) -> {
                    try {
                        AbstractShader shader = cls.getConstructor().newInstance();
                        SHADER_MAP.put(cls, shader);
                    } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
                        LogHelper.error(e);
                        System.exit(0);
                    }
                });
    }

    public <T extends AbstractShader> T getShader(Class<T> shaderClass) {
        return shaderClass.cast(SHADER_MAP.get(shaderClass));
    }

    public AbstractShader getShader(String shaderName) {
        for (AbstractShader shader : SHADER_MAP.values())
            if (shader.getId().equalsIgnoreCase(shaderName))
                return shader;
        return null;
    }

    public void reloadShaders() {
        SHADER_MAP.values().forEach(AbstractShader::reload);
    }

}
