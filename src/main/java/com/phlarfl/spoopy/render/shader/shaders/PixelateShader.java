package com.phlarfl.spoopy.render.shader.shaders;

import com.phlarfl.spoopy.render.shader.AbstractShader;
import org.lwjgl.opengl.ARBShaderObjects;

public class PixelateShader extends AbstractShader {

    @Override
    protected String getId() {
        return "pixelate";
    }

    public void bind(float amount) {
        super.bind();

        int locSample = getLocation("sample");
        int locResolution = getLocation("resolution");
        int locAmount = getLocation("texel");

        ARBShaderObjects.glUniform1iARB(locSample, 0);
        ARBShaderObjects.glUniform2fARB(locResolution, 1f / mc.mainWindow.getWidth(), 1f / mc.mainWindow.getHeight());
        ARBShaderObjects.glUniform1fARB(locAmount, amount);
    }

}
