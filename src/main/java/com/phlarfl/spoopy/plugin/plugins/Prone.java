package com.phlarfl.spoopy.plugin.plugins;

import com.phlarfl.spoopy.eventbus.EventFactory;
import com.phlarfl.spoopy.eventbus.events.EventEntityIsPoseClear;
import com.phlarfl.spoopy.eventbus.events.EventPlayerGetIsSwimming;
import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import net.minecraft.entity.Pose;

@PluginDescriptor(id = "prone")
public class Prone extends AbstractPlugin {

    public Prone() {
        super();

        EventFactory.INSTANCE.register(EventEntityIsPoseClear.class, 0, this::onEventEntityIsPoseClear);
        EventFactory.INSTANCE.register(EventPlayerGetIsSwimming.class, 0, this::onEventPlayerGetIsSwimming);
    }

    private void onEventEntityIsPoseClear(EventEntityIsPoseClear event) {
        if (enabled && event.getPose() == Pose.SWIMMING) {
            event.setPoseClear(true);
            event.setCanceled(true);
        }
    }

    private void onEventPlayerGetIsSwimming(EventPlayerGetIsSwimming event) {
        if (enabled) {
            event.setSwimming(true);
            event.setCanceled(true);
        }
    }

}
