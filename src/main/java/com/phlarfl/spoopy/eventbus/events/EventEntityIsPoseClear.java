package com.phlarfl.spoopy.eventbus.events;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.Pose;
import net.minecraftforge.eventbus.api.Event;

@Getter
@Setter
public class EventEntityIsPoseClear extends Event {

    private final Entity entity;
    private final Pose pose;
    private boolean isPoseClear;

    public EventEntityIsPoseClear(Entity entity, Pose pose, boolean isPoseClear) {
        this.entity = entity;
        this.pose = pose;
        this.isPoseClear = isPoseClear;
    }

    @Override
    public boolean isCancelable() {
        return true;
    }
}
