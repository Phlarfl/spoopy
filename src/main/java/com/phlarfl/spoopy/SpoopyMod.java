package com.phlarfl.spoopy;

import com.phlarfl.spoopy.command.CommandFactory;
import com.phlarfl.spoopy.plugin.PluginFactory;
import com.phlarfl.spoopy.render.font.FontFactory;
import com.phlarfl.spoopy.render.shader.ShaderFactory;
import com.phlarfl.spoopy.util.LocaleHelper;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(SpoopyMod.MOD_ID)
public class SpoopyMod {

    public static final String MOD_ID = "spoopy";
    public static final String MOD_NAME = LocaleHelper.format("mod.name");

    public SpoopyMod() {
        Minecraft.getInstance().execute(() -> {
            LocaleHelper.register();

            FontFactory.register();
            ShaderFactory.INSTANCE.register();
            PluginFactory.INSTANCE.register();
            CommandFactory.INSTANCE.register();

            Runtime.getRuntime().addShutdownHook(new Thread(() -> PluginFactory.INSTANCE.getPlugins().forEach(PluginFactory.INSTANCE::storeConfig)));
        });
    }

}
