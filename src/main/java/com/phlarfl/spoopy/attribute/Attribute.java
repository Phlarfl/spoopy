package com.phlarfl.spoopy.attribute;

import com.google.common.base.Strings;
import com.phlarfl.spoopy.attribute.exception.AttributeException;
import com.phlarfl.spoopy.attribute.type.AttributeType;
import com.phlarfl.spoopy.util.LocaleHelper;

import java.lang.reflect.Field;

public class Attribute {

    private AbstractAttributeFactory factory;
    private AttributeDescriptor descriptor;
    private Attribute parent;
    private Field field;

    private String name, description;

    public Attribute(AbstractAttributeFactory factory, Field field) {
        this.factory = factory;
        this.field = field;
        descriptor = field.getAnnotation(AttributeDescriptor.class);
        name = LocaleHelper.format(String.format("%s.name", descriptor.property()));
        description = LocaleHelper.format(String.format("%s.description", descriptor.property()));
    }

    public AbstractAttributeFactory getFactory() {
        return factory;
    }

    public String getParentId() {
        return descriptor.parentId();
    }

    public String getId() {
        return (Strings.isNullOrEmpty(getParentId()) ? "" : getParentId() + ".") + descriptor.id();
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Attribute getParent() {
        return parent;
    }

    public void setParent(Attribute parent) {
        this.parent = parent;
    }

    public Object getValue() throws IllegalAccessException {
        return field.get(factory);
    }

    public void setValue(String value) throws IllegalArgumentException, InvalidAttributeType, AttributeException {
        AttributeType type = AttributeType.getType(field.getType());
        if (type != null) type.setValue(factory, field, value);
        else throw new InvalidAttributeType();
    }

}
