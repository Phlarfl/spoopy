package com.phlarfl.spoopy.render.font;

import com.google.common.collect.HashBasedTable;
import com.phlarfl.spoopy.SpoopyMod;
import com.phlarfl.spoopy.util.ImageHelper;
import com.phlarfl.spoopy.util.MinecraftHelper;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.NativeImage;
import net.minecraft.util.ResourceLocation;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class FontBuilder implements MinecraftHelper {

    private static final Font DEFAULT = new Font("Calibri", Font.PLAIN, 12);
    private static final Color COLOR_EMPTY = new Color(255, 255, 255, 0);
    private static final HashBasedTable<String, Integer, CustomFont> FONT_CACHE = HashBasedTable.create();

    private String name, asset;
    private int size;
    private boolean bold, custom;
    private Font font = DEFAULT;
    private BufferedImage image = new BufferedImage(512, 512, BufferedImage.TYPE_INT_ARGB);
    private FontMetrics fontMetrics;
    private Glyph[] glyphs = new Glyph[256];

    public FontBuilder setName(String name) {
        this.name = name;
        this.asset = String.format("/assets/spoopy/font/%s", name);
        return this;
    }

    public FontBuilder setSize(int size) {
        this.size = size;
        return this;
    }

    public FontBuilder setBold(boolean bold) {
        this.bold = bold;
        return this;
    }

    public FontBuilder setCustom(boolean custom) {
        this.custom = custom;
        return this;
    }

    public CustomFont build() {
        if (FONT_CACHE.contains(name, size))
            return FONT_CACHE.get(name, size);
        if (custom)
            try {
                font = Font.createFont(Font.TRUETYPE_FONT, FontBuilder.class.getResourceAsStream(String.format("%s.ttf", asset))).deriveFont(bold ? Font.BOLD : Font.PLAIN, size);
            } catch (FontFormatException | IOException e) {
                e.printStackTrace();
            }
        else font = new Font(name, bold ? Font.BOLD : Font.PLAIN, size);

        Graphics2D graphics = buildGraphics();
        fontMetrics = graphics.getFontMetrics();

        int fontHeight = renderCharacters(graphics);

        try {
            DynamicTexture dynamicTexture = new DynamicTexture(NativeImage.read(ImageHelper.toInputStream(image)));
            ResourceLocation resource = mc.getTextureManager().getDynamicTextureLocation(String.format("%s/fonts/%s.%s", SpoopyMod.MOD_ID.toLowerCase(), name, size), dynamicTexture);
            CustomFont customFont = new CustomFont(resource, fontHeight, glyphs);
            FONT_CACHE.put(font.getName(), size, customFont);
            return customFont;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;//new CustomFont(name, size, bold, custom);
    }

    private Graphics2D buildGraphics() {
        Graphics2D graphics = (Graphics2D) image.getGraphics();
        graphics.setFont(font);
        graphics.setColor(COLOR_EMPTY);
        graphics.fillRect(0, 0, 512, 512);
        graphics.setColor(Color.WHITE);
        graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        return graphics;
    }

    private int renderCharacters(Graphics2D graphics) {
        int fontHeight, charHeight, posX, posY;
        fontHeight = charHeight = posX = posY = 0;

        for (int i = 0; i < 256; i++) {
            char c = (char) i;
            String ch = String.valueOf(c);
            Rectangle2D rect = fontMetrics.getStringBounds(ch, graphics);
            Rectangle bounds = rect.getBounds();
            int w = bounds.width + 8;
            int h = bounds.height;
            if (posX + w >= 512) {
                posX = 0;
                posY += charHeight;
                charHeight = 0;
            }
            charHeight = Math.max(charHeight, h);
            fontHeight = Math.max(fontHeight, charHeight);
            glyphs[i] = new Glyph(c, posX, posY, w, h, w - 8, 512, 512);
            graphics.drawString(ch, posX + 2, posY + fontMetrics.getAscent());
            posX += w;
        }
        return fontHeight;
    }

}
