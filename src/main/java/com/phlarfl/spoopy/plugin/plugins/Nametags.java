package com.phlarfl.spoopy.plugin.plugins;

import com.mojang.blaze3d.platform.GlStateManager;
import com.phlarfl.spoopy.attribute.AttributeDescriptor;
import com.phlarfl.spoopy.entity.EntityFactory;
import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import com.phlarfl.spoopy.render.RenderState;
import com.phlarfl.spoopy.render.font.CustomFont;
import com.phlarfl.spoopy.render.font.FontFactory;
import com.phlarfl.spoopy.render.geometry.Rect;
import com.phlarfl.spoopy.util.EntityHelper;
import com.phlarfl.spoopy.util.MathHelper;
import com.phlarfl.spoopy.util.RenderHelper;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

@PluginDescriptor(id = "nametags")
public class Nametags extends AbstractPlugin {

    //@formatter:off

    @AttributeDescriptor(id = "entities", property = "plugin.nametags.attribute.entities")
    public boolean entities = true;

        @AttributeDescriptor(parentId = "entities", id = "bosses", property = "plugin.nametags.attribute.entities.bosses")
        public boolean entitiesBosses = true;

        @AttributeDescriptor(parentId = "entities", id = "items", property = "plugin.nametags.attribute.entities.items")
        public boolean entitiesItems = true;

        @AttributeDescriptor(parentId = "entities", id = "merchants", property = "plugin.nametags.attribute.entities.merchants")
        public boolean entitiesMerchants = true;

        @AttributeDescriptor(parentId = "entities", id = "monsters", property = "plugin.nametags.attribute.entities.monsters")
        public boolean entitiesMonsters = true;

        @AttributeDescriptor(parentId = "entities", id = "passives", property = "plugin.nametags.attribute.entities.passives")
        public boolean entitiesPassives = true;

        @AttributeDescriptor(parentId = "entities", id = "players", property = "plugin.nametags.attribute.entities.players")
        public boolean entitiesPlayers = true;

        @AttributeDescriptor(parentId = "entities", id = "projectiles", property = "plugin.nametags.attribute.entities.projectiles")
        public boolean entitiesProjectiles = true;

    //@formatter:on

    private transient CustomFont font = FontFactory.CALIBRI_14.getFont();
    private transient SortedMap<Entity, Vec3d> positions = new TreeMap<>((a, b) -> -Double.compare(a.getDistanceSq(mc.player), b.getDistanceSq(mc.player)));

    @SubscribeEvent
    public void onRenderWorldLastEvent(RenderWorldLastEvent event) {
        if (enabled) {
            for (Entity entity : mc.world.getAllEntities())
                if (isRenderable(entity))
                    positions.put(entity, RenderHelper.worldToScreen(EntityHelper.getRenderPosition(entity, mc.getRenderPartialTicks()).add(0, entity.getHeight() + 0.4, 0)));
        }
    }

    @SubscribeEvent
    public void onRenderGameOverlayEvent(RenderGameOverlayEvent.Pre event) {
        if (enabled && event.getType() == RenderGameOverlayEvent.ElementType.ALL) {

            double scx = mc.mainWindow.getWidth() / 2.;
            double scy = mc.mainWindow.getHeight() / 2.;
            double max = Math.hypot(scx, scy);
            double tagHeight = font.getHeight() / 2.;
            double tagPaddingH = 4;
            double tagPaddingV = 2;

            GlStateManager.pushMatrix();
            double scale = mc.mainWindow.getGuiScaleFactor() / Math.pow(mc.mainWindow.getGuiScaleFactor(), 2);
            GlStateManager.scaled(scale, scale, scale);
            RenderState state = new RenderState(Rect.DEFAULT_RENDER_STATES);
            state.store();

            for (Entity entity : positions.keySet()) {
                Vec3d pos = positions.get(entity);
                if (pos != null) {
                    double x = pos.getX();
                    double y = pos.getY();
                    boolean inverse = pos.getZ() > 1;
                    double angle = Math.toRadians(MathHelper.wrap(MathHelper.angle(scx, scy, pos.getX(), pos.getY()) + (inverse ? 180 : 0), 360));
                    double distance = inverse ? max : Math.min(Math.hypot(scx - x, scy - y), max);

                    x = scx + (Math.cos(angle) * distance);
                    y = scy + (Math.sin(angle) * distance);

                    x = Math.round(x);
                    y = Math.round(y);

                    String name = entity.getDisplayName().getFormattedText();
                    double tagWidth = font.getWidth(name) / 2.;

                    Rect.draw_Xy2_Col(x - tagWidth - tagPaddingH, y - tagHeight - tagPaddingV, x + tagWidth + tagPaddingH, y + tagHeight + tagPaddingV, 0x80000000, 1);
                    font.drawString(name, x - tagWidth, y - tagHeight, -1, false);
                }
            }
            state.restore();
            GlStateManager.popMatrix();

            positions.clear();
        }
    }

    private boolean isRenderable(Entity entity) {
        if (!entities) return false;
        switch (EntityFactory.INSTANCE.getType(entity)) {
            case BOSS:
                return entitiesBosses;
            case ITEM:
                return entitiesItems;
            case MERCHANT:
                return entitiesMerchants;
            case MONSTER:
                return entitiesMonsters;
            case PASSIVE:
                return entitiesPassives;
            case PLAYER:
                return entitiesPlayers;
            case PROJECTILE:
                return entitiesProjectiles;
            case UNKNOWN:
            default:
                return false;
        }
    }

}
