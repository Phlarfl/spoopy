package com.phlarfl.spoopy.util;

public class ColorHelper {

    private enum Pigment {
        R(16),
        G(8),
        B(0),
        A(24);

        private int offset;

        Pigment(int offset) {
            this.offset = offset;
        }

        public int getOffset() {
            return offset;
        }
    }

    private static int getPigment(long color, Pigment pigment) {
        return (int) (color >> pigment.getOffset()) & 255;
    }

    private static int putPigmentf(float part, Pigment pigment) {
        return (int) (part * 255f) << pigment.getOffset();
    }

    private static int putPigmenti(int part, Pigment pigment) {
        return part << pigment.getOffset();
    }

    public static float[] getColor4f(long color) {
        float[] out = new float[4];
        for (Pigment pigment : Pigment.values())
            out[pigment.ordinal()] = getPigment(color, pigment) / 255f;
        return out;
    }

    public static int[] getColor4i(long color) {
        int[] out = new int[4];
        for (Pigment pigment : Pigment.values())
            out[pigment.ordinal()] = getPigment(color, pigment);
        return out;
    }

    public static int getColor(float[] color) {
        int out = putPigmentf(color[0], Pigment.R);
        for (int i = 1; i < Pigment.values().length; i++)
            out |= putPigmentf(color[i], Pigment.values()[i]);
        return out;
    }

    public static int getColor(int[] color) {
        int out = putPigmenti(color[0], Pigment.R);
        for (int i = 1; i < Pigment.values().length; i++)
            out |= putPigmenti(color[i], Pigment.values()[i]);
        return out;
    }

    public static String toHex(int color) {
        return "#" + Integer.toHexString(color).toUpperCase();
    }

    public static int setAlpha(int color, int alpha) {
        return (color & 0x00FFFFFF) | putPigmenti(alpha, Pigment.A);
    }

    public static int blend(int ca, int cb, double blend) {
        double ar = (ca >> 16 & 255) / 255.;
        double ag = (ca >> 8 & 255) / 255.;
        double ab = (ca & 255) / 255.;
        double aa = (ca >> 24 & 255) / 255.;

        double br = (cb >> 16 & 255) / 255.;
        double bg = (cb >> 8 & 255) / 255.;
        double bb = (cb & 255) / 255.;
        double ba = (cb >> 24 & 255) / 255.;

        blend = Math.min(Math.max(blend, 0), 1);
        double am2 = 1 - blend;

        double r = (ar * am2) + (br * blend);
        double g = (ag * am2) + (bg * blend);
        double b = (ab * am2) + (bb * blend);
        double a = (aa * am2) + (ba * blend);

        return (((int) (r * 255) & 255) << 16) | (((int) (g * 255) & 255) << 8) | (((int) (b * 255) & 255)) | (((int) (a * 255) & 255) << 24);
    }

}
