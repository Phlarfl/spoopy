package com.phlarfl.spoopy.render.geometry;

import com.mojang.blaze3d.platform.GlStateManager;
import com.phlarfl.spoopy.render.RenderState;
import com.phlarfl.spoopy.util.ColorHelper;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import org.lwjgl.opengl.GL11;

public class Line {

    public static final RenderState.State[] DEFAULT_RENDER_STATES = {
            RenderState.State.LIGHTING, RenderState.State.TEXTURE, RenderState.State.TEX_GEN, RenderState.State.ALPHA, RenderState.State.BLEND, RenderState.State.SHADE_MODEL, RenderState.State.LINE_WIDTH
    };

    private static final int[] D_COL = {-1, -1};
    private static final float D_WID = 1;

    public static void draw_Xyz_Col2_Wid(double x1, double y1, double z1, double x2, double y2, double z2, int color1, int color2, float width, double alpha) {
        if (width <= 0 || alpha == 0 || (x1 == x2 && y1 == y2 && z1 == z2))
            return;

        int[][] rgba = new int[][]{
                ColorHelper.getColor4i(color1),
                ColorHelper.getColor4i(color2)
        };

        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder bufferbuilder = tessellator.getBuffer();
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.disableTexture();
        GlStateManager.enableAlphaTest();
        GlStateManager.alphaFunc(GL11.GL_GREATER, 0.001f);
        GlStateManager.enableBlend();
        GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
        GlStateManager.lineWidth(width);
        GlStateManager.shadeModel(GL11.GL_SMOOTH);
        RenderHelper.disableStandardItemLighting();
        bufferbuilder.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION_COLOR);
        bufferbuilder.pos(x1, y1, z1).color(rgba[0][0], rgba[0][1], rgba[0][2], (int) (rgba[0][3] * alpha)).endVertex();
        bufferbuilder.pos(x2, y2, z2).color(rgba[1][0], rgba[1][1], rgba[1][2], (int) (rgba[1][3] * alpha)).endVertex();
        tessellator.draw();
        RenderHelper.enableStandardItemLighting();
    }

    public static void draw_Xyz(double x1, double y1, double z1, double x2, double y2, double z2, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, D_COL[0], D_COL[1], D_WID, alpha);
    }

    public static void draw_Xyz_Col2(double x1, double y1, double z1, double x2, double y2, double z2, int color1, int color2, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, color1, color2, D_WID, alpha);
    }

    public static void draw_Xyz_Col(double x1, double y1, double z1, double x2, double y2, double z2, int color, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, color, color, D_WID, alpha);
    }

    public static void draw_Xyz_Col_Wid(double x1, double y1, double z1, double x2, double y2, double z2, int color, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, color, color, width, alpha);
    }

    public static void draw_Xyz_Wid(double x1, double y1, double z1, double x2, double y2, double z2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, z1, x2, y2, z2, D_COL[0], D_COL[1], width, alpha);
    }

    public static void draw_Xy(double x1, double y1, double x2, double y2, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, D_COL[0], D_COL[1], D_WID, alpha);
    }

    public static void draw_Xy_Col2(double x1, double y1, double x2, double y2, int color1, int color2, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, color1, color2, D_WID, alpha);
    }

    public static void draw_Xy_Col2_Wid(double x1, double y1, double x2, double y2, int color1, int color2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, color1, color2, width, alpha);
    }

    public static void draw_Xy_Col(double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, color, color, D_WID, alpha);
    }

    public static void draw_Xy_Col_Wid(double x1, double y1, double x2, double y2, int color, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, color, color, width, alpha);
    }

    public static void draw_Xy_Wid(double x1, double y1, double x2, double y2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, y1, 0, x2, y2, 0, D_COL[0], D_COL[1], width, alpha);
    }

    public static void draw_Xz(double x1, double z1, double x2, double z2, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, D_COL[0], D_COL[1], D_WID, alpha);
    }

    public static void draw_Xz_Col2(double x1, double z1, double x2, double z2, int color1, int color2, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, color1, color2, D_WID, alpha);
    }

    public static void draw_Xz_Col2_Wid(double x1, double z1, double x2, double z2, int color1, int color2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, color1, color2, width, alpha);
    }

    public static void draw_Xz_Col(double x1, double z1, double x2, double z2, int color, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, color, color, D_WID, alpha);
    }

    public static void draw_Xz_Col_Wid(double x1, double z1, double x2, double z2, int color, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, color, color, width, alpha);
    }

    public static void draw_Xz_Wid(double x1, double z1, double x2, double z2, float width, double alpha) {
        draw_Xyz_Col2_Wid(x1, 0, z1, x2, 0, z2, D_COL[0], D_COL[1], width, alpha);
    }

    public static void draw_Yz(double y1, double z1, double y2, double z2, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, D_COL[0], D_COL[1], D_WID, alpha);
    }

    public static void draw_Yz_Col2(double y1, double z1, double y2, double z2, int color1, int color2, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, color1, color2, D_WID, alpha);
    }

    public static void draw_Yz_Col2_Wid(double y1, double z1, double y2, double z2, int color1, int color2, float width, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, color1, color2, width, alpha);
    }

    public static void draw_Yz_Col(double y1, double z1, double y2, double z2, int color, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, color, color, D_WID, alpha);
    }

    public static void draw_Yz_Col_Wid(double y1, double z1, double y2, double z2, int color, float width, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, color, color, width, alpha);
    }

    public static void draw_Yz_Wid(double y1, double z1, double y2, double z2, float width, double alpha) {
        draw_Xyz_Col2_Wid(0, y1, z1, 0, y2, z2, D_COL[0], D_COL[1], width, alpha);
    }

}
