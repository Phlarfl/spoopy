package com.phlarfl.spoopy.render.shader.shaders;

import com.phlarfl.spoopy.render.shader.AbstractShader;
import org.lwjgl.opengl.ARBShaderObjects;

public class OutlineShader extends AbstractShader {

    @Override
    protected String getId() {
        return "outline";
    }

    public void bind(int size, boolean glow, float divisible, boolean glowOutline) {
        super.bind();

        int locSample = getLocation("sample");
        int locTexel = getLocation("texel");
        int locSize = getLocation("size");
        int locGlow = getLocation("glow");
        int locRatio = getLocation("ratio");
        int locDivisible = getLocation("divisible");
        int locGlowOutline = getLocation("glowOutline");

        ARBShaderObjects.glUniform1iARB(locSample, 0);
        ARBShaderObjects.glUniform2fARB(locTexel, 1f / mc.mainWindow.getWidth(), 1f / mc.mainWindow.getHeight());
        ARBShaderObjects.glUniform2fARB(locSize, size, size);
        ARBShaderObjects.glUniform1iARB(locGlow, glow ? 1 : 0);
        ARBShaderObjects.glUniform1fARB(locDivisible, divisible);
        ARBShaderObjects.glUniform1iARB(locGlowOutline, glowOutline ? 1 : 0);
    }

}
