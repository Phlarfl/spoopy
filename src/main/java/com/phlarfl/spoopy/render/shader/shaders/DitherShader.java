package com.phlarfl.spoopy.render.shader.shaders;

import com.phlarfl.spoopy.render.shader.AbstractShader;
import org.lwjgl.opengl.ARBShaderObjects;

public class DitherShader extends AbstractShader {

    @Override
    protected String getId() {
        return "dither";
    }

    public void bind() {
        super.bind();

        int locSample = getLocation("sample");

        ARBShaderObjects.glUniform1iARB(locSample, 0);
    }

}
