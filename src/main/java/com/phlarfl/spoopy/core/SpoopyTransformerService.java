package com.phlarfl.spoopy.core;

import cpw.mods.modlauncher.api.IEnvironment;
import cpw.mods.modlauncher.api.ITransformationService;
import cpw.mods.modlauncher.api.ITransformer;
import net.minecraftforge.coremod.CoreModEngine;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;

public class SpoopyTransformerService implements ITransformationService {

    private CoreModEngine engine = new CoreModEngine();

    @Nonnull
    @Override
    public String name() {
        return "spoopyCoreMod";
    }

    @Override
    public void initialize(IEnvironment environment) {
    }

    @Override
    public void beginScanning(IEnvironment environment) {
    }

    @Override
    public void onLoad(IEnvironment env, Set<String> otherServices) {
    }

    @Nonnull
    @Override
    public List<ITransformer> transformers() {
        List<ITransformer<?>> transformers = engine.initializeCoreMods();
        transformers.addAll(new SpoopyCoreMod().buildTransformers());
        return (List) transformers;
    }
}
