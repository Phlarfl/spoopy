package com.phlarfl.spoopy.core.transformer;

import com.phlarfl.spoopy.core.transformer.descriptor.ClassDescriptor;
import com.phlarfl.spoopy.core.transformer.descriptor.DescribedMethod;
import cpw.mods.modlauncher.api.ITransformer;
import cpw.mods.modlauncher.api.ITransformerVotingContext;
import cpw.mods.modlauncher.api.TransformerVoteResult;
import javassist.*;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractClassTransformer implements ITransformer<ClassNode> {

    private final Set<ITransformer.Target> targets = new HashSet<>();
    protected final Set<DescribedMethod> methods = new HashSet<>();

    protected AbstractClassTransformer() {
        ClassDescriptor classDescriptor = getClass().getAnnotation(ClassDescriptor.class);
        if (classDescriptor != null)
            for (String cls : classDescriptor.value())
                targets.add(ITransformer.Target.targetClass(cls));
        init();
    }

    protected abstract void init();

    @Nonnull
    @Override
    public ClassNode transform(ClassNode input, ITransformerVotingContext context) {
        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        input.accept(writer);
        byte[] ba = writer.toByteArray();

        ClassPool pool = ClassPool.getDefault();
        try {
            CtClass cls = pool.makeClass(new ByteArrayInputStream(ba));

            methods.forEach((describedMethod) -> {
                try {
                    CtMethod method = cls.getMethod(describedMethod.getMethod(), describedMethod.getDesc());
                    switch (describedMethod.getInsertType()) {
                        case BEFORE:
                            method.insertBefore(describedMethod.getCode());
                            break;
                        case AFTER:
                            method.insertAfter(describedMethod.getCode());
                            break;
                        case AT:
                            method.insertAt(describedMethod.getInsertAt(), describedMethod.getCode());
                            break;
                    }
                } catch (NotFoundException | CannotCompileException e) {
                    e.printStackTrace();
                }
            });
            ba = cls.toBytecode();
        } catch (IOException | CannotCompileException e) {
            e.printStackTrace();
            ba = writer.toByteArray();
        }

        ClassNode node = new ClassNode();
        ClassReader reader = new ClassReader(ba);
        reader.accept(node, 0);
        return node;
    }

    @Nonnull
    @Override
    public TransformerVoteResult castVote(ITransformerVotingContext context) {
        return TransformerVoteResult.YES;
    }

    @Nonnull
    @Override
    public Set<Target> targets() {
        return targets;
    }

}
