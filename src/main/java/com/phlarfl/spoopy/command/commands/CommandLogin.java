package com.phlarfl.spoopy.command.commands;

import com.mojang.authlib.Agent;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;
import com.phlarfl.spoopy.command.Command;
import com.phlarfl.spoopy.command.exception.ArgumentCountException;
import com.phlarfl.spoopy.command.exception.CommandException;
import com.phlarfl.spoopy.util.ChatHelper;
import com.phlarfl.spoopy.util.ClassHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.Session;

import java.lang.reflect.Field;
import java.net.Proxy;

public class CommandLogin implements Command {

    @Override
    public String getCommandId() {
        return "login";
    }

    @Override
    public int getCommandMinArgs() {
        return 1;
    }

    @Override
    public int getCommandMaxArgs() {
        return 2;
    }

    @Override
    public void executeCommand(String[] args) throws CommandException {
        if (args.length == 1 || args.length == 2)
            ChatHelper.print("command.login.exec.as", args[0]);
        try {
            switch (args.length) {
                case 1:
                    setSession(new Session(args[0], args[0], args[0], "legacy"));
                    ChatHelper.print("command.login.exec.as.success", args[0]);
                    break;
                case 2:
                    YggdrasilUserAuthentication auth = (YggdrasilUserAuthentication) new YggdrasilAuthenticationService(Proxy.NO_PROXY, "1").createUserAuthentication(Agent.MINECRAFT);
                    auth.setUsername(args[0]);
                    auth.setPassword(args[1]);
                    auth.logIn();
                    if (auth.getSelectedProfile() != null) {
                        setSession(new Session(auth.getSelectedProfile().getName(), auth.getSelectedProfile().getId().toString(), auth.getAuthenticatedToken(), "legacy"));
                        ChatHelper.print("command.login.exec.as.success", args[0]);
                    } else ChatHelper.print("command.login.exec.as.failed", args[0]);
                    break;
                default:
                    throw new ArgumentCountException(getCommandMinArgs(), getCommandMaxArgs());
            }
        } catch (AuthenticationException | IllegalAccessException e) {
            ChatHelper.print("command.login.exec.as.failed", args[0], e.getLocalizedMessage());
        }
    }

    private void setSession(Session session) throws IllegalAccessException {
        Field[] fields = ClassHelper.getFieldsByType(Minecraft.class.getDeclaredFields(), Session.class);
        if (fields.length > 0) {
            Field field = fields[0];
            field.setAccessible(true);
            field.set(mc, session);
            field.setAccessible(false);
        }
    }

}
