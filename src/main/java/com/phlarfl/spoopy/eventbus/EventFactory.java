package com.phlarfl.spoopy.eventbus;

import net.minecraftforge.eventbus.api.Event;

import java.util.*;
import java.util.function.Consumer;

public enum EventFactory {
    INSTANCE;

    private final Map<Class<? extends Event>, List<Consumer>> EVENT_MAP = new HashMap<>();
    private final Map<Consumer, Integer> CONSUMER_PRIORITY_MAP = new HashMap<>();

    public <T extends Event> void register(Class<T> eventClass, int priority, Consumer<T> consumer) {
        List<Consumer> consumers = EVENT_MAP.get(eventClass);
        if (!EVENT_MAP.containsKey(eventClass))
            EVENT_MAP.put(eventClass, consumers = new ArrayList<>());
        consumers.add(consumer);
        CONSUMER_PRIORITY_MAP.put(consumer, priority);
    }

    public <T extends Event> void deregister(Class<T> eventClass, Consumer<T> consumer) {
        if (!EVENT_MAP.containsKey(eventClass))
            return;
        EVENT_MAP.get(eventClass).remove(consumer);
    }

    public <T extends Event> void handleEvent(T event) {
        if (!EVENT_MAP.containsKey(event.getClass()))
            return;
        EVENT_MAP.get(event.getClass()).stream().sorted(Comparator.comparing(a -> CONSUMER_PRIORITY_MAP.getOrDefault(a, 0))).forEach((consumer) -> consumer.accept(event));
    }
}
