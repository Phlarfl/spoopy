package com.phlarfl.spoopy.eventbus.events;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.Entity;
import net.minecraftforge.eventbus.api.Event;

@Getter
@Setter
public class EventPlayerSlowDown extends Event {

    private final Entity entity;

    public EventPlayerSlowDown(Entity entity) {
        this.entity = entity;
    }
}
