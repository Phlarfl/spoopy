package com.phlarfl.spoopy.render.font;

import com.mojang.blaze3d.platform.GlStateManager;
import com.phlarfl.spoopy.render.RenderState;
import com.phlarfl.spoopy.render.geometry.Line;
import com.phlarfl.spoopy.render.geometry.Rect;
import com.phlarfl.spoopy.util.ColorHelper;
import com.phlarfl.spoopy.util.MinecraftHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import org.apache.commons.lang3.StringUtils;
import org.lwjgl.opengl.GL11;

import java.util.stream.IntStream;

public class CustomFont implements MinecraftHelper {

    private static final int[] COLORS = new int[32];
    private static final String COLOR_CODES = "0123456789abcdefklmnor";

    static {
        for (int i = 0; i < COLORS.length; i++) {
            int v = (i >> 3 & 1) * 85;
            int r = (i >> 2 & 1) * 170 + v;
            int g = (i >> 1 & 1) * 170 + v;
            int b = (i & 1) * 170 + v;
            if (i == 6) r += 85;
            if (i >= 16) {
                r /= 4;
                g /= 4;
                b /= 4;
            }
            COLORS[i] = (r & 255) << 16 | (g & 255) << 8 | b & 255 | 255 << 24;
        }
    }

    private ResourceLocation resource;
    private int fontHeight;
    private Glyph[] glyphs;
    private double offsetX, offsetY;

    public CustomFont(ResourceLocation resource, int fontHeight, Glyph[] glyphs) {
        this.resource = resource;
        this.fontHeight = fontHeight;
        this.glyphs = glyphs;
    }

    public CustomFont setOffset(double offsetX, double offsetY) {
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        return this;
    }

    public int getWidth(String text) {
        text = TextFormatting.getTextWithoutFormattingCodes(text);
        if (text == null) return 0;
        int width = 0;
        String[] split = text.split("\n");
        for (String s : split)
            width = Math.max(width, IntStream.range(0, s.length()).map((i) -> glyphs[s.charAt(i)].getW() - 8).sum());
        return width;
    }

    public int getHeight() {
        return fontHeight;
    }

    public int getHeight(String text) {
        text = TextFormatting.getTextWithoutFormattingCodes(text);
        return fontHeight * (StringUtils.countMatches(text, "\n") + 1);
    }

    public void drawString(String text, double x, double y, int color, boolean shadow) {
        x += 2.1;
        y += 1.1;

        if (text.contains("\n")) {
            String[] lines = text.split("\n");
            double _h = getHeight(text) - 16;
            double _y = y - (_h / 2.);
            for (String line : lines) {
                drawString(line, x, _y, color, shadow);
                _y += getHeight(line);
            }
            return;
        }

        double offsetX = this.offsetX;
        double offsetY = this.offsetY;

        int[] rgba = ColorHelper.getColor4i(color);

        RenderState state = new RenderState(Rect.DEFAULT_RENDER_STATES);
        state.store();

        GlStateManager.pushMatrix();
        GlStateManager.translated(x + offsetX, y + offsetY, 0);

        x = 0;
        y = 0;
        char[] chars = text.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            CharacterFormat format = new CharacterFormat(chars[i], 15, rgba);
            if (format.isFormatted(i, chars)) {
                i++;
                continue;
            }

            Glyph glyph = glyphs[format.getCharacter()];
            boolean square = false;
            if (glyph == null) {
                glyph = glyphs['A'];
                square = true;
            }

            Glyph o = glyph;
            if (format.isMagic() && !square) {
                int a;
                do a = (int) (Math.random() * glyphs.length);
                while (glyphs[a].getW() != glyph.getW());
                glyph = glyphs[a];
            }

            double w = glyph.getW();
            double h = glyph.getH();
            double _x = x - ((w - getWidth(String.valueOf(format.getCharacter()))) / 2.);
            double _y = y - ((h - getHeight()) / 2.);
            float tx = glyph.getTx();
            float ty = glyph.getTy();
            float tw = glyph.getTw();
            float th = glyph.getTh();
            float italic = format.isItalic() ? 4 : 0;

            for (int j = shadow ? 1 : 0; j >= 0; j--) {
                for (int k = 0; k < (format.isBold() ? 2 : 1); k++) {
                    int renderColor = ColorHelper.getColor(rgba);
                    if (j == 1) {
                        renderColor = COLORS[format.getFontColor() + 16];
                        int[] renderRgba = ColorHelper.getColor4i(renderColor);
                        renderRgba[3] = rgba[3];
                        renderColor = ColorHelper.blend(ColorHelper.getColor(renderRgba), ColorHelper.setAlpha(0xFF000000, rgba[3]), 0.5);
                    }
                    GlStateManager.pushMatrix();
                    GlStateManager.translated(j + k, j, 0);

                    if (square) {
                        int a = 1;
                        Rect.draw_Xy4_Col_Mod(_x + italic + a, _y + a, _x + w + italic - a, _y + a, _x + w - a, _y + h - a, _x + a, _y + h - a, renderColor, GL11.GL_LINE_LOOP, 1);
                    } else
                        Rect.draw_Res_Xy4_Tex2_Col(resource, _x + italic, _y, _x + w + italic, _y, _x + w, _y + h, _x, _y + h, tx, ty, tx + tw, ty + th, renderColor, 1);
                    if (format.isStrike())
                        Rect.draw_Xy2_Col(_x, _y + (h / 2.) - 1, _x + w, _y + (h / 2.) + 1, renderColor, 1);
                    if (format.isUnderline())
                        Rect.draw_Xy2_Col(_x, _y + (h * 0.85) - 2, _x + w, _y + (h * 0.85), renderColor, 1);

                    GlStateManager.popMatrix();
                }
            }
            x += o.getA();
        }

        GlStateManager.popMatrix();
        state.restore();
    }

    private class CharacterFormat {

        private char character;
        private int fontColor;
        private int[] rgba;
        private boolean magic, bold, strike, underline, italic;

        public CharacterFormat(char character, int fontColor, int[] rgba) {
            this.character = character;
            this.fontColor = fontColor;
            this.rgba = rgba;
        }

        public char getCharacter() {
            return character;
        }

        public int getFontColor() {
            return fontColor;
        }

        public int[] getRgba() {
            return rgba;
        }

        public boolean isMagic() {
            return magic;
        }

        public boolean isBold() {
            return bold;
        }

        public boolean isStrike() {
            return strike;
        }

        public boolean isUnderline() {
            return underline;
        }

        public boolean isItalic() {
            return italic;
        }

        public boolean isFormatted(int i, char[] chars) {
            if (character == '\247' && i < chars.length - 1) {
                int index = COLOR_CODES.indexOf(chars[i + 1]);
                if (index != -1) {
                    switch (index) {
                        case 16:
                            magic = true;
                            break;
                        case 17:
                            bold = true;
                            break;
                        case 18:
                            strike = true;
                            break;
                        case 19:
                            underline = true;
                            break;
                        case 20:
                            italic = true;
                            break;
                        case 21:
                        default:
                            magic = bold = strike = underline = italic = false;
                            fontColor = index == 21 ? 15 : index;
                            break;
                    }
                    int _color = COLORS[fontColor];
                    int alpha = rgba[3];
                    rgba = ColorHelper.getColor4i(_color);
                    rgba[3] = alpha;
                    return true;
                }
            }
            return false;
        }
    }

}
