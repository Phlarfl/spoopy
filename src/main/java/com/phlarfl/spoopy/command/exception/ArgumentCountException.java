package com.phlarfl.spoopy.command.exception;

import com.phlarfl.spoopy.util.LocaleHelper;
import com.phlarfl.spoopy.util.OpsHelper;

public class ArgumentCountException extends CommandException {

    private int min, max;

    public ArgumentCountException(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public String getMin() {
        return String.valueOf(OpsHelper.elseOr(min, -1, LocaleHelper.format("commands.error.arguments.count.min")));
    }

    public String getMax() {
        return String.valueOf(OpsHelper.elseOr(max, -1, LocaleHelper.format("commands.error.arguments.count.max")));
    }

}
