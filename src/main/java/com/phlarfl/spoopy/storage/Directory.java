package com.phlarfl.spoopy.storage;

import java.util.Arrays;

public enum Directory {
    CONFIG("config"),
    PLUGIN("plugin", CONFIG);

    private String directory;

    Directory(String directory, Directory... parent) {
        this(String.format("%s/%s", String.join("/", Arrays.stream(parent).map(Directory::getDirectory).toArray(String[]::new)), directory));
    }

    Directory(String directory) {
        this.directory = directory;
    }

    public String getDirectory() {
        return directory;
    }
}
