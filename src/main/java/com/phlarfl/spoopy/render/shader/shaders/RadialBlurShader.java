package com.phlarfl.spoopy.render.shader.shaders;

import com.phlarfl.spoopy.render.shader.AbstractShader;
import org.lwjgl.opengl.ARBShaderObjects;

public class RadialBlurShader extends AbstractShader {

    @Override
    protected String getId() {
        return "radialblur";
    }

    @Override
    public void bind() {
        super.bind();

        int locExposure = getLocation("exposure");
        int locDecay = getLocation("decay");
        int locDensity = getLocation("density");
        int locWeight = getLocation("weight");
        int locResolution = getLocation("resolution");
        int locLightPositionOnScreen = getLocation("lightPositionOnScreen");
        int locFirstPass = getLocation("firstPass");
        int locSamples = getLocation("samples");

        ARBShaderObjects.glUniform1fARB(locExposure, 0.95f);
        ARBShaderObjects.glUniform1fARB(locDecay, 0.95f);
        ARBShaderObjects.glUniform1fARB(locDensity, 0.95f);
        ARBShaderObjects.glUniform1fARB(locWeight, 0.5f);
        ARBShaderObjects.glUniform2fARB(locResolution, mc.mainWindow.getWidth(), mc.mainWindow.getHeight());
        ARBShaderObjects.glUniform2fARB(locLightPositionOnScreen, mc.mainWindow.getWidth() / 2f, mc.mainWindow.getHeight() / 2f);
        ARBShaderObjects.glUniform1iARB(locFirstPass, 0);
        ARBShaderObjects.glUniform1iARB(locSamples, 200);
    }

}
