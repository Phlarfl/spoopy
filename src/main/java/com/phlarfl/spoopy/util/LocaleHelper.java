package com.phlarfl.spoopy.util;

import com.phlarfl.spoopy.SpoopyMod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class LocaleHelper {

    private enum Color {
        MUNDANE("&m", "\2477"),
        KEYWORD("&k", "\247d"),
        VALUE("&v", "\247e");

        private String key, value;

        Color(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }

    private static final String LANG_FILE = String.format("/assets/%s/translation/en.properties", SpoopyMod.MOD_ID);
    private static final Map<String, String> LOCALE_MAP = new HashMap<>();
    private static boolean registered;

    public static void register() {
        if (registered)
            return;
        registered = true;
        BufferedReader br = new BufferedReader(new InputStreamReader(LocaleHelper.class.getResourceAsStream(LANG_FILE)));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                int ind = line.indexOf("=");
                if (line.startsWith("#") || ind == -1)
                    continue;
                LOCALE_MAP.put(line.substring(0, ind), formatColor(line.substring(ind + 1)));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String formatColor(String message) {
        for (Color c : Color.values())
            message = message.replaceAll(c.getKey(), c.getValue());
        return message;
    }

    public static String format(String translateKey, Object... parameters) {
        register();
        String format = OpsHelper.ternary(LOCALE_MAP.containsKey(translateKey), LOCALE_MAP.get(translateKey), translateKey);
        return (parameters != null && parameters.length > 0) ? String.format(format, parameters) : format;
    }

}
