package com.phlarfl.spoopy.storage;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.phlarfl.spoopy.SpoopyMod;
import com.phlarfl.spoopy.util.FileHelper;
import net.minecraft.client.Minecraft;

import java.io.File;
import java.io.IOException;

public enum StorageFactory {
    INSTANCE;

    private final File CONFIG_DIR = new File(Minecraft.getInstance().gameDir, SpoopyMod.MOD_ID);
    private final Gson GSON = new Gson();
    private final ObjectMapper MAPPER = new ObjectMapper()
            .configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true)
            .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private File getFile(String... path) throws IOException {
        File file = new File(CONFIG_DIR, String.join("/", path));
        if (!file.exists()) {
            if (!file.getParentFile().exists() && !file.getParentFile().mkdirs())
                throw new IOException("Failed to create config file directory: " + file.getParentFile().getAbsolutePath());
            if (!file.createNewFile())
                throw new IOException("Failed to create config file: " + file.getAbsolutePath());
        }
        return file;
    }

    public <T> void load(T instance, Directory directory, String fileName) throws IOException {
        MAPPER.readerForUpdating(instance).readValue(FileHelper.readFile(getFile(directory.getDirectory(), fileName)));
    }

    public <T> void store(T instance, Directory directory, String fileName) throws IOException {
        FileHelper.writeFile(getFile(directory.getDirectory(), fileName), GSON.toJson(instance));
    }

}
