package com.phlarfl.spoopy.command.exception;

public class InvalidArgumentException extends CommandException {

    private String argument;

    public InvalidArgumentException(String argument) {
        this.argument = argument;
    }

    public String getArgument() {
        return argument;
    }

}
