package com.phlarfl.spoopy.command;

import com.phlarfl.spoopy.command.exception.CommandException;
import com.phlarfl.spoopy.util.LocaleHelper;
import com.phlarfl.spoopy.util.MinecraftHelper;

public interface Command extends MinecraftHelper {

    String getCommandId();

    default String getCommandName() {
        return LocaleHelper.format(String.format("command.%s.name", getCommandId()));
    }

    default String getCommandDescription() {
        return LocaleHelper.format(String.format("command.%s.description", getCommandId()));
    }

    default String getCommandAlias() {
        return LocaleHelper.format(String.format("command.%s.alias", getCommandId()));
    }

    default String getCommandUsage() {
        return LocaleHelper.format(String.format("command.%s.usage", getCommandId()));
    }

    int getCommandMinArgs();

    int getCommandMaxArgs();

    void executeCommand(String[] args) throws CommandException;

}
