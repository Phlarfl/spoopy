package com.phlarfl.spoopy.core;

import net.minecraft.client.renderer.chunk.ChunkRender;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.Entity;

public class AsmHandler {

    public static void preRenderChunk(ChunkRender chunkRender) {
        System.out.println("preRenderChunk");
    }

    public static void setOrigin() {
        System.out.println("setOrigin");
    }

    public static void init() {
        System.out.println("Initialize mod");
    }

    public static void onEntityRender(EntityRendererManager rendererManager, Entity entity, double x, double y, double z, float yaw, float partial) {
        System.out.println(rendererManager + " : " + entity + " : " + x + " : " + y + " : " + z + " : " + yaw);
    }

}
