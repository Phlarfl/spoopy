package com.phlarfl.spoopy.render.shader;

import com.google.common.base.Strings;
import com.phlarfl.spoopy.SpoopyMod;
import com.phlarfl.spoopy.util.FileHelper;
import com.phlarfl.spoopy.util.MinecraftHelper;
import org.lwjgl.opengl.ARBFragmentShader;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.ARBVertexShader;
import org.lwjgl.opengl.GL11;

import java.util.function.Supplier;

public abstract class AbstractShader implements MinecraftHelper {

    private boolean inUse;
    private int program;

    protected AbstractShader() {
        reload();
    }

    protected abstract String getId();

    protected int getProgram() {
        return program;
    }

    protected int getLocation(String location) {
        return ARBShaderObjects.glGetUniformLocationARB(getProgram(), location);
    }

    private int createShader(String fileContent, int shaderType) {
        if (Strings.isNullOrEmpty(fileContent))
            return 0;

        int shader = 0;
        try {
            shader = ARBShaderObjects.glCreateShaderObjectARB(shaderType);
            if (shader == 0)
                return 0;

            ARBShaderObjects.glShaderSourceARB(shader, fileContent);
            ARBShaderObjects.glCompileShaderARB(shader);

            if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE)
                throw new Exception(String.format("Error creating shader %s", ARBShaderObjects.glGetInfoLogARB(shader, ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB))));

            return shader;
        } catch (Exception e) {
            ARBShaderObjects.glDeleteObjectARB(shader);
            throw new RuntimeException(e);
        }
    }

    public void reload() {
        reload(() -> FileHelper.readResource(String.format("/assets/%s/shader/%s/vgl", SpoopyMod.MOD_ID, getId())),
                () -> FileHelper.readResource(String.format("/assets/%s/shader/%s/fgl", SpoopyMod.MOD_ID, getId())));
    }

    public void reload(Supplier<String> vertex, Supplier<String> fragment) {
        int vs = createShader(vertex.get(), ARBVertexShader.GL_VERTEX_SHADER_ARB);
        int fs = createShader(fragment.get(), ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);

        if (vs == 0 || fs == 0 || (program = ARBShaderObjects.glCreateProgramObjectARB()) == 0)
            throw new RuntimeException(String.format("Failed to load shader %s", getId()));

        ARBShaderObjects.glAttachObjectARB(getProgram(), vs);
        ARBShaderObjects.glAttachObjectARB(getProgram(), fs);
        ARBShaderObjects.glLinkProgramARB(getProgram());

        if (ARBShaderObjects.glGetObjectParameteriARB(getProgram(), ARBShaderObjects.GL_OBJECT_LINK_STATUS_ARB) == GL11.GL_FALSE)
            throw new RuntimeException(String.format("Failed to load shader %s", getId()));
        ARBShaderObjects.glValidateProgramARB(getProgram());
        if (ARBShaderObjects.glGetObjectParameteriARB(getProgram(), ARBShaderObjects.GL_OBJECT_VALIDATE_STATUS_ARB) == GL11.GL_FALSE)
            throw new RuntimeException(String.format("Failed to load shader %s", getId()));

        inUse = true;
    }

    public void bind() {
        if (inUse)
            ARBShaderObjects.glUseProgramObjectARB(getProgram());
    }

    public void unbind() {
        if (inUse)
            ARBShaderObjects.glUseProgramObjectARB(0);
    }

}
