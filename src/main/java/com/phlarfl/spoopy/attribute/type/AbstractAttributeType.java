package com.phlarfl.spoopy.attribute.type;

public interface AbstractAttributeType<T extends AbstractAttributeType<T>> {

    boolean isValid(String value);

    T parse(String value);

}
