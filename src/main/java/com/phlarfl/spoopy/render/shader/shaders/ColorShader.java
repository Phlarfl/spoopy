package com.phlarfl.spoopy.render.shader.shaders;

import com.phlarfl.spoopy.render.shader.AbstractShader;
import com.phlarfl.spoopy.util.ColorHelper;
import org.lwjgl.opengl.ARBShaderObjects;

public class ColorShader extends AbstractShader {

    @Override
    protected String getId() {
        return "color";
    }

    public void bind(long color) {
        super.bind();

        int locSample = getLocation("sample");
        int locColor = getLocation("color");

        float[] color4f = ColorHelper.getColor4f(color);

        ARBShaderObjects.glUniform1iARB(locSample, 0);
        ARBShaderObjects.glUniform4fARB(locColor, color4f[0], color4f[1], color4f[2], color4f[3]);
    }

}
