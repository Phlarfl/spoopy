package com.phlarfl.spoopy.util.callback;

public interface TriFunction<T, U, V, W> {
    W accept(T t, U u, V v);
}
