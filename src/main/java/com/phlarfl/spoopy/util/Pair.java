package com.phlarfl.spoopy.util;

import lombok.Getter;

@Getter
public class Pair<T, U> {

    private T t;
    private U u;

    public Pair(T t, U u) {
        this.t = t;
        this.u = u;
    }

}
