package com.phlarfl.spoopy.util;

import com.phlarfl.spoopy.SpoopyMod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogHelper {

    private enum LogType {
        DEBUG,
        INFO,
        WARN,
        ERROR
    }

    private static final Logger LOGGER = LogManager.getLogger();

    private static void log(LogType logType, String i18n, Object... args) {
        i18n = LocaleHelper.format("util.log.print.format", SpoopyMod.MOD_NAME, LocaleHelper.format(i18n, args), false);
        switch (logType) {
            case DEBUG:
                LOGGER.debug(i18n);
                break;
            case INFO:
                LOGGER.info(i18n);
                break;
            case WARN:
                LOGGER.warn(i18n);
                break;
            case ERROR:
                LOGGER.error(i18n);
                break;
        }
    }

    public static void debug(String i18n, Object... args) {
        log(LogType.DEBUG, i18n, args);
    }

    public static void info(String i18n, Object... args) {
        log(LogType.INFO, i18n, args);
    }

    public static void warn(String i18n, Object... args) {
        log(LogType.WARN, i18n, args);
    }

    public static void error(Throwable throwable) {
        if (throwable.getMessage() != null)
            log(LogType.ERROR, throwable.getMessage());
        throwable.printStackTrace();
    }

}
