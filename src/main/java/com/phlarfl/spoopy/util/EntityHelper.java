package com.phlarfl.spoopy.util;

import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;

public class EntityHelper implements MinecraftHelper {

    public static Vec3d getCameraPosition() {
        return mc.gameRenderer.getActiveRenderInfo().getProjectedView();
    }

    public static Vec3d getRenderPosition(Entity entity, float partialTicks) {
        return entity.getEyePosition(partialTicks).subtract(0, entity.getEyeHeight(), 0);
    }

}
