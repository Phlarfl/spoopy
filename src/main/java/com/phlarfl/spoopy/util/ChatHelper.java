package com.phlarfl.spoopy.util;

import com.phlarfl.spoopy.SpoopyMod;
import net.minecraft.util.text.ChatType;
import net.minecraft.util.text.StringTextComponent;

public class ChatHelper implements MinecraftHelper {

    public static void print(String i18n, Object... args) {
        i18n = LocaleHelper.format("util.chat.print.format", SpoopyMod.MOD_NAME, LocaleHelper.format(i18n, args));
        if (mc.ingameGUI != null)
            mc.ingameGUI.addChatMessage(ChatType.SYSTEM, new StringTextComponent(i18n));
    }

}
