package com.phlarfl.spoopy.core.transformer;

import com.phlarfl.spoopy.core.transformer.descriptor.ClassDescriptor;
import com.phlarfl.spoopy.core.transformer.descriptor.DescribedMethod;
import com.phlarfl.spoopy.core.transformer.descriptor.InsertType;
import cpw.mods.modlauncher.api.ITransformer;
import org.objectweb.asm.tree.ClassNode;

@ClassDescriptor("net.minecraft.entity.player.PlayerEntity")
public class PlayerEntityTransformer extends AbstractClassTransformer implements ITransformer<ClassNode> {

    @Override
    public void init() {
        methods.add(new DescribedMethod("isSwimming", "()Z", InsertType.BEFORE, () -> {
            return "com.phlarfl.spoopy.eventbus.events.EventPlayerGetIsSwimming event = new com.phlarfl.spoopy.eventbus.events.EventPlayerGetIsSwimming($0, !this.abilities.isFlying && !this.isSpectator() && super.isSwimming());" +
                    "com.phlarfl.spoopy.eventbus.EventFactory.INSTANCE.handleEvent(event);" +
                    "if (event.isCanceled()) return event.isSwimming();";
        }));
    }
}
