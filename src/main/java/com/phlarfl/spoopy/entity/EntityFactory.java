package com.phlarfl.spoopy.entity;

import com.phlarfl.spoopy.util.Pair;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;

import java.util.ArrayList;
import java.util.List;

public enum EntityFactory {
    INSTANCE;

    private final List<Pair<Class<? extends Entity>, EntityType>> ENTITY_TYPES = new ArrayList<Pair<Class<? extends Entity>, EntityType>>() {{
        putType(this, EntityType.values());
    }};

    private final List<Pair<Class<? extends TileEntity>, TileEntityType>> TILE_ENTITY_TYPES = new ArrayList<Pair<Class<? extends TileEntity>, TileEntityType>>() {{
        putType(this, TileEntityType.values());
    }};

    private <T, U extends Type<T>> void putType(List<Pair<Class<? extends T>, U>> list, U[] e) {
        for (U u : e)
            for (Class<? extends T> cls : u.getClasses())
                list.add(new Pair<>(cls, u));
    }

    public EntityType getType(Entity entity) {
        for (Pair<Class<? extends Entity>, EntityType> pair : ENTITY_TYPES)
            if (pair.getT().isInstance(entity))
                return pair.getU();
        return EntityType.UNKNOWN;
    }

    public TileEntityType getType(TileEntity entity) {
        for (Pair<Class<? extends TileEntity>, TileEntityType> pair : TILE_ENTITY_TYPES)
            if (pair.getT().isInstance(entity))
                return pair.getU();
        return TileEntityType.UNKNOWN;
    }

}
