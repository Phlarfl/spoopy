package com.phlarfl.spoopy.render.font;

public enum FontFactory {
    CALIBRI_12("calibri", 12, false),
    CALIBRI_14("calibri", 14, false),
    CALIBRI_16("calibri", 16, false),
    CALIBRI_18("calibri", 18, false),
    CALIBRI_20("calibri", 20, false);

    private String key;
    private int size;
    private boolean custom;

    FontFactory(String key, int size, boolean custom) {
        this.key = key;
        this.size = size;
        this.custom = custom;
    }

    public CustomFont getFont() {
        return new FontBuilder().setName(key).setSize(size).setCustom(custom).build();
    }

    public static void register() {
        for (FontFactory font : values())
            font.getFont();
    }
}
