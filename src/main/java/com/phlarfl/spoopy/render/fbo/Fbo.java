package com.phlarfl.spoopy.render.fbo;

import com.phlarfl.spoopy.util.MinecraftHelper;
import net.minecraft.client.Minecraft;

public class Fbo implements MinecraftHelper {

    private HFramebuffer framebuffer;
    private int scale;

    public Fbo(int scale, boolean depth) {
        this.scale = scale;
        framebuffer = new HFramebuffer(mc.mainWindow.getWidth() * scale, mc.mainWindow.getHeight() * scale, depth, Minecraft.IS_RUNNING_ON_MAC);
    }

    public HFramebuffer getFramebuffer() {
        return framebuffer;
    }

    public void setColor(float r, float g, float b, float a) {
        if (getWidth() != mc.mainWindow.getWidth() * scale || getHeight() != mc.mainWindow.getHeight())
            onResized();
        getFramebuffer().setFramebufferColor(r, g, b, a);
        getFramebuffer().framebufferClear(Minecraft.IS_RUNNING_ON_MAC);
    }

    public int getWidth() {
        return getFramebuffer().framebufferWidth;
    }

    public int getHeight() {
        return getFramebuffer().framebufferHeight;
    }

    private void onResized() {
        getFramebuffer().func_216491_a(mc.mainWindow.getWidth() * scale, mc.mainWindow.getHeight() * scale, Minecraft.IS_RUNNING_ON_MAC);
        unbind();
    }

    public void bind() {
        FboFactory.INSTANCE.bind(true);
        getFramebuffer().bindFramebuffer(true);
    }

    public void unbind() {
        getFramebuffer().unbindFramebuffer();
        mc.getFramebuffer().bindFramebuffer(true);
        FboFactory.INSTANCE.bind(false);
    }


}
