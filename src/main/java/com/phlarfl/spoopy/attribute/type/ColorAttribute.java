package com.phlarfl.spoopy.attribute.type;

import com.phlarfl.spoopy.util.ColorHelper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ColorAttribute implements AbstractAttributeType<ColorAttribute> {

    private int color;

    public ColorAttribute() {
    }

    public ColorAttribute(int color) {
        this.color = color;
    }

    @Override
    public boolean isValid(String value) {
        return parse(value).color == color;
    }

    @Override
    public ColorAttribute parse(String value) {
        return new ColorAttribute((int) Long.parseLong(value, 16));
    }

    @Override
    public String toString() {
        return ColorHelper.toHex(color);
    }

}
