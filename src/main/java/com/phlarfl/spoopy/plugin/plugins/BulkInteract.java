package com.phlarfl.spoopy.plugin.plugins;

import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import net.minecraft.entity.Entity;
import net.minecraft.network.play.client.CUseEntityPacket;
import net.minecraft.util.Hand;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@PluginDescriptor(id = "bulk_interact")
public class BulkInteract extends AbstractPlugin {

    @SubscribeEvent
    public void onRightClickItemEvent(PlayerInteractEvent.RightClickItem event) {
        if (enabled && mc.getConnection() != null) {
            for (Entity entity : mc.world.getAllEntities()) {
                if (Math.sqrt(mc.player.getDistanceSq(entity)) < 5) {
                    mc.getConnection().sendPacket(new CUseEntityPacket(entity, Hand.MAIN_HAND));
                }
            }
        }
    }

}
