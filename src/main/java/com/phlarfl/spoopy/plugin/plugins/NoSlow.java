package com.phlarfl.spoopy.plugin.plugins;

import com.phlarfl.spoopy.eventbus.EventFactory;
import com.phlarfl.spoopy.eventbus.events.EventMovementInput;
import com.phlarfl.spoopy.eventbus.events.EventPlayerSlowDown;
import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@PluginDescriptor(id = "noslow")
public class NoSlow extends AbstractPlugin {

    public NoSlow() {
        super();

        EventFactory.INSTANCE.register(EventMovementInput.class, 0, this::onEventMovementInput);
    }

    private void onEventMovementInput(EventMovementInput event) {
        if (enabled)
            event.setNoDampening(true);
    }

    @SubscribeEvent
    public void onInputUpdateEvent(InputUpdateEvent event) {
        if (enabled && event.getPlayer() == mc.player && event.getPlayer().isHandActive() && !event.getPlayer().isPassenger()) {
            event.getMovementInput().moveStrafe *= 5F;
            event.getMovementInput().moveForward *= 5F;
        }
    }

}
