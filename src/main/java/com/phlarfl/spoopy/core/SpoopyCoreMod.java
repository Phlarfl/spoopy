package com.phlarfl.spoopy.core;

import com.phlarfl.spoopy.core.transformer.AbstractClassTransformer;
import com.phlarfl.spoopy.util.ClassHelper;
import cpw.mods.modlauncher.api.ITransformer;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class SpoopyCoreMod {

    public List<ITransformer<?>> buildTransformers() {
        List<ITransformer<?>> transformers = new ArrayList<>();

        new Reflections(ClassHelper.getPackageName(AbstractClassTransformer.class))
                .getSubTypesOf(AbstractClassTransformer.class)
                .forEach((cls) -> {
                    try {
                        ITransformer transformer = cls.getConstructor().newInstance();
                        transformers.add(transformer);
                    } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                });

        return transformers;
    }

}
