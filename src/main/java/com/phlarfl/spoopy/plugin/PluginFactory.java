package com.phlarfl.spoopy.plugin;

import com.phlarfl.spoopy.storage.Directory;
import com.phlarfl.spoopy.storage.StorageFactory;
import com.phlarfl.spoopy.util.ClassHelper;
import com.phlarfl.spoopy.util.LogHelper;
import org.reflections.Reflections;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public enum PluginFactory {
    INSTANCE;

    private final Map<Class<? extends AbstractPlugin>, AbstractPlugin> PLUGIN_MAP = new HashMap<>();

    public void register() {
        new Reflections(ClassHelper.getPackageName(AbstractPlugin.class))
                .getSubTypesOf(AbstractPlugin.class)
                .forEach((cls) -> {
                    if (cls.isAnnotationPresent(PluginDescriptor.class) && !ClassHelper.isAbstractOrInterface(cls)) {
                        try {
                            AbstractPlugin plugin = cls.getConstructor().newInstance();
                            register(plugin);
                        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
                            LogHelper.error(e);
                        }
                    } else
                        LogHelper.debug("plugins.init.error.notAnnotated", AbstractPlugin.class.getSimpleName(), PluginDescriptor.class.getSimpleName());
                });
    }

    private void register(AbstractPlugin plugin) {
        PLUGIN_MAP.put(plugin.getClass(), plugin);
        loadConfig(plugin);
        storeConfig(plugin);
    }

    public void loadConfig(AbstractPlugin plugin) {
        try {
            StorageFactory.INSTANCE.load(plugin, Directory.PLUGIN, plugin.name);
        } catch (IOException e) {
            LogHelper.warn("plugins.init.error.config.fail.load", plugin.name);
            LogHelper.error(e);
        }
    }

    public void storeConfig(AbstractPlugin plugin) {
        try {
            StorageFactory.INSTANCE.store(plugin, Directory.PLUGIN, plugin.name);
        } catch (IOException e) {
            LogHelper.warn("plugins.init.error.config.fail.store", plugin.name);
            LogHelper.error(e);
        }
    }

    public Collection<AbstractPlugin> getPlugins() {
        return PLUGIN_MAP.values();
    }

    public <T extends AbstractPlugin> T getPlugin(Class<T> pluginClass) {
        return pluginClass.cast(PLUGIN_MAP.get(pluginClass));
    }

}
