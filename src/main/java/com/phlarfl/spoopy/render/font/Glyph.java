package com.phlarfl.spoopy.render.font;

public class Glyph {

    private char c;
    private int x, y, w, h, a;
    private float tx, ty, tw, th;

    public Glyph(char c, int x, int y, int w, int h, int a, int tw, int th) {
        setC(c);
        setX(x);
        setY(y);
        setW(w);
        setH(h);
        setA(a);
        setTx(1f / tw * x);
        setTy(1f / th * y);
        setTw(1f / tw * w);
        setTh(1f / th * h);
    }

    public char getC() {
        return c;
    }

    public void setC(char c) {
        this.c = c;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getW() {
        return w;
    }

    public void setW(int w) {
        this.w = w;
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public float getTx() {
        return tx;
    }

    public void setTx(float tx) {
        this.tx = tx;
    }

    public float getTy() {
        return ty;
    }

    public void setTy(float ty) {
        this.ty = ty;
    }

    public float getTw() {
        return tw;
    }

    public void setTw(float tw) {
        this.tw = tw;
    }

    public float getTh() {
        return th;
    }

    public void setTh(float th) {
        this.th = th;
    }
}
