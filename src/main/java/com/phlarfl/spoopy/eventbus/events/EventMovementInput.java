package com.phlarfl.spoopy.eventbus.events;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.Entity;
import net.minecraftforge.eventbus.api.Event;

@Getter
@Setter
public class EventMovementInput extends Event {

    private boolean slow, noDampening;

    public EventMovementInput(boolean slow, boolean noDampening) {
        this.slow = slow;
        this.noDampening = noDampening;
    }

    @Override
    public boolean isCancelable() {
        return true;
    }
}
