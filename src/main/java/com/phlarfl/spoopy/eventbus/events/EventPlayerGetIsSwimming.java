package com.phlarfl.spoopy.eventbus.events;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.entity.Entity;
import net.minecraftforge.eventbus.api.Event;

@Getter
@Setter
public class EventPlayerGetIsSwimming extends Event {

    private final Entity entity;
    private boolean isSwimming;

    public EventPlayerGetIsSwimming(Entity entity, boolean isSwimming) {
        this.entity = entity;
        this.isSwimming = isSwimming;
    }

    @Override
    public boolean isCancelable() {
        return true;
    }
}
