package com.phlarfl.spoopy.plugin.plugins;

import com.mojang.blaze3d.platform.GlStateManager;
import com.phlarfl.spoopy.attribute.AttributeDescriptor;
import com.phlarfl.spoopy.attribute.type.ColorAttribute;
import com.phlarfl.spoopy.entity.EntityFactory;
import com.phlarfl.spoopy.eventbus.EventFactory;
import com.phlarfl.spoopy.eventbus.events.EventRenderEntity;
import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import com.phlarfl.spoopy.render.fbo.Fbo;
import com.phlarfl.spoopy.render.fbo.FboFactory;
import com.phlarfl.spoopy.render.shader.ShaderFactory;
import com.phlarfl.spoopy.render.shader.shaders.ColorShader;
import com.phlarfl.spoopy.render.shader.shaders.OutlineShader;
import net.minecraft.entity.Entity;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@PluginDescriptor(id = "esp")
public class Esp extends AbstractPlugin {

    //@formatter:off

    @AttributeDescriptor(id = "glow", property = "plugin.esp.attribute.glow")
    public boolean glow = true;

        @AttributeDescriptor(parentId = "glow", id = "outline", property = "plugin.esp.attribute.glow.outline")
        public boolean glowOutline = true;

    @AttributeDescriptor(id = "width", property = "plugin.esp.attribute.width")
    public int width = 3;

    @AttributeDescriptor(id = "entities", property = "plugin.esp.attribute.entities")
    public boolean entities = true;

        @AttributeDescriptor(parentId = "entities", id = "bosses", property = "plugin.esp.attribute.entities.bosses")
        public boolean entitiesBosses = true;

            @AttributeDescriptor(parentId = "entities.bosses", id = "color", property = "plugin.esp.attribute.entities.all.color")
            public ColorAttribute entitiesBossesColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "items", property = "plugin.esp.attribute.entities.items")
        public boolean entitiesItems = true;

            @AttributeDescriptor(parentId = "entities.items", id = "color", property = "plugin.esp.attribute.entities.all.color")
            public ColorAttribute entitiesItemsColor = new ColorAttribute(0xFFB080E0);

        @AttributeDescriptor(parentId = "entities", id = "merchants", property = "plugin.esp.attribute.entities.merchants")
        public boolean entitiesMerchants = true;

            @AttributeDescriptor(parentId = "entities.merchants", id = "color", property = "plugin.esp.attribute.entities.all.color")
            public ColorAttribute entitiesMerchantsColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "monsters", property = "plugin.esp.attribute.entities.monsters")
        public boolean entitiesMonsters = true;

            @AttributeDescriptor(parentId = "entities.monsters", id = "color", property = "plugin.esp.attribute.entities.all.color")
            public ColorAttribute entitiesMonstersColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "passives", property = "plugin.esp.attribute.entities.passives")
        public boolean entitiesPassives = true;

            @AttributeDescriptor(parentId = "entities.passives", id = "color", property = "plugin.esp.attribute.entities.all.color")
            public ColorAttribute entitiesPassivesColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "players", property = "plugin.esp.attribute.entities.players")
        public boolean entitiesPlayers = true;

            @AttributeDescriptor(parentId = "entities.players", id = "color", property = "plugin.esp.attribute.entities.all.color")
            public ColorAttribute entitiesPlayersColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "projectiles", property = "plugin.esp.attribute.entities.projectiles")
        public boolean entitiesProjectiles = true;

            @AttributeDescriptor(parentId = "entities.players", id = "color", property = "plugin.esp.attribute.entities.all.color")
            public ColorAttribute entitiesProjectilesColor = new ColorAttribute(0xFFFF8000);

    //@formatter:on

    private transient Fbo modelBuffer, outlineShaderBuffer;

    private transient ColorShader colorShader;
    private transient OutlineShader outlineShader;

    public Esp() {
        super();

        modelBuffer = FboFactory.INSTANCE.register(getClass(), "model_buffer", 1, true);
        outlineShaderBuffer = FboFactory.INSTANCE.register(getClass(), "outline_shader_buffer", 1, true);

        colorShader = ShaderFactory.INSTANCE.getShader(ColorShader.class);
        outlineShader = ShaderFactory.INSTANCE.getShader(OutlineShader.class);

        EventFactory.INSTANCE.register(EventRenderEntity.class, 0, this::onRenderEntityEvent);
    }

    private void onRenderEntityEvent(EventRenderEntity event) {
        if (enabled && isRenderable(event.getEntity())) {
            enabled = false;
            modelBuffer.bind();
            colorShader.bind(getColor(event.getEntity()));
            event.getEntityRendererManager().setRenderOutlines(true);
            event.getEntityRenderer().doRender(event.getEntity(), event.getX(), event.getY(), event.getZ(), 0, mc.getRenderPartialTicks());
            event.getEntityRendererManager().setRenderOutlines(false);
            colorShader.unbind();
            modelBuffer.unbind();
            enabled = true;
        }
    }

    @SubscribeEvent
    public void onRenderGameOverlayEvent(RenderGameOverlayEvent.Pre event) {
        if (enabled && event.getType() == RenderGameOverlayEvent.ElementType.ALL) {
            GlStateManager.pushMatrix();
            double scale = mc.mainWindow.getGuiScaleFactor() / Math.pow(mc.mainWindow.getGuiScaleFactor(), 2);
            GlStateManager.scaled(scale, scale, scale);

            outlineShaderBuffer.setColor(0, 0, 0, 0);
            outlineShaderBuffer.bind();

            if (outlineShader != null) outlineShader.bind(width, glow, (float) Math.pow(width, 2), glowOutline);
            modelBuffer.getFramebuffer().framebufferRenderExt(modelBuffer.getWidth(), modelBuffer.getHeight(), true);
            if (outlineShader != null) outlineShader.unbind();

            outlineShaderBuffer.unbind();

            outlineShaderBuffer.getFramebuffer().framebufferRenderExt(outlineShaderBuffer.getWidth(), outlineShaderBuffer.getHeight(), false);

            modelBuffer.setColor(0, 0, 0, 0);
            modelBuffer.unbind();

            GlStateManager.popMatrix();
        }
    }

    private boolean isRenderable(Entity entity) {
        if (!entities) return false;
        switch (EntityFactory.INSTANCE.getType(entity)) {
            case BOSS:
                return entitiesBosses;
            case ITEM:
                return entitiesItems;
            case MERCHANT:
                return entitiesMerchants;
            case MONSTER:
                return entitiesMonsters;
            case PASSIVE:
                return entitiesPassives;
            case PLAYER:
                return entitiesPlayers;
            case PROJECTILE:
                return entitiesProjectiles;
            case UNKNOWN:
            default:
                return false;
        }
    }

    private int getColor(Entity entity) {
        switch (EntityFactory.INSTANCE.getType(entity)) {
            case BOSS:
                return entitiesBossesColor.getColor();
            case ITEM:
                return entitiesItemsColor.getColor();
            case MERCHANT:
                return entitiesMerchantsColor.getColor();
            case MONSTER:
                return entitiesMonstersColor.getColor();
            case PASSIVE:
                return entitiesPassivesColor.getColor();
            case PLAYER:
                return entitiesPlayersColor.getColor();
            case PROJECTILE:
                return entitiesProjectilesColor.getColor();
            case UNKNOWN:
            default:
                return 0;
        }
    }

}
