package com.phlarfl.spoopy.render.texture;

import com.phlarfl.spoopy.SpoopyMod;
import com.phlarfl.spoopy.util.MinecraftHelper;
import net.minecraft.client.renderer.texture.DynamicTexture;

import java.util.HashMap;
import java.util.Map;

public class TextureCache implements MinecraftHelper {

    private static final Map<String, Texture> TEXTURE_CACHE = new HashMap<>();

    public static Texture download(String url) {
        Texture texture = TEXTURE_CACHE.get(url);
        if (texture != null) {
            if (texture.isDownloaded())
                texture.setResource(mc.getTextureManager().getDynamicTextureLocation(String.format("%s/%s", SpoopyMod.MOD_ID.toLowerCase(), url.replaceAll("[^a-z0-9_.-]", "_")), new DynamicTexture(texture.getImage())));
            return texture;
        }
        TEXTURE_CACHE.put(url, texture = new Texture(url));
        texture.download();
        return texture;
    }

}
