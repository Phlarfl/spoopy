package com.phlarfl.spoopy.util;

import java.io.*;
import java.util.stream.Collectors;

public class FileHelper {

    public static String readResource(String path) {
        InputStream stream = FileHelper.class.getResourceAsStream(path);
        return stream == null ? null : readBuffer(new InputStreamReader(stream));
    }

    private static String readBuffer(InputStreamReader stream) {
        return stream == null ? null : new BufferedReader(stream).lines().collect(Collectors.joining("\n"));
    }

    public static String readFile(String path) throws FileNotFoundException {
        FileReader fr = new FileReader(path);
        BufferedReader br = new BufferedReader(fr);
        return br.lines().collect(Collectors.joining("\n"));
    }

    public static String readFile(File file) throws FileNotFoundException {
        return file != null && file.exists() ? readFile(file.getAbsolutePath()) : null;
    }

    public static void writeFile(String path, String content) throws IOException {
        Writer writer = new FileWriter(path);
        writeFile(writer, content);
    }

    public static void writeFile(File file, String content) throws IOException {
        if (file != null && file.exists())
            writeFile(file.getAbsolutePath(), content);
    }

    private static void writeFile(Writer writer, String content) throws IOException {
        writer.append(content);
        writer.flush();
        writer.close();
    }

}
