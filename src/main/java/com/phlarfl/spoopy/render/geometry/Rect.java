package com.phlarfl.spoopy.render.geometry;

import com.mojang.blaze3d.platform.GlStateManager;
import com.phlarfl.spoopy.render.RenderState;
import com.phlarfl.spoopy.render.texture.Texture;
import com.phlarfl.spoopy.render.texture.TextureCache;
import com.phlarfl.spoopy.util.ColorHelper;
import com.phlarfl.spoopy.util.MinecraftHelper;
import joptsimple.internal.Strings;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class Rect implements MinecraftHelper {

    public static final RenderState.State[] DEFAULT_RENDER_STATES = {
            RenderState.State.LIGHTING, RenderState.State.TEXTURE, RenderState.State.TEX_GEN, RenderState.State.ALPHA, RenderState.State.BLEND, RenderState.State.SHADE_MODEL, RenderState.State.LINE_WIDTH
    };

    private static final ResourceLocation D_RES = null;
    private static final String D_URL = null;
    private static final double[] D_TEX = {0, 0, 1, 0, 1, 1, 0, 1};
    private static final int[] D_COL = {-1, -1, -1, -1};
    private static final int D_MOD = GL11.GL_TRIANGLE_FAN;

    public static void draw_Res_Url_Xyz4_Tex4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        if (alpha == 0 || (x1 == x2 && x2 == x3 && x3 == x4 && y1 == y2 && y2 == y3 && y3 == y4 && z1 == z2 && z2 == z3 && z3 == z4))
            return;

        int[][] rgba = new int[][]{
                ColorHelper.getColor4i(color1),
                ColorHelper.getColor4i(color2),
                ColorHelper.getColor4i(color3),
                ColorHelper.getColor4i(color4)
        };

        boolean gettingResource = false;
        if (!Strings.isNullOrEmpty(url)) {
            Texture texture = TextureCache.download(url);
            if (texture.getResource() != null)
                resource = texture.getResource();
            gettingResource = true;
        }

        if (resource == null)
            GlStateManager.disableTexture();
        else {
            mc.getTextureManager().bindTexture(resource);
            GlStateManager.enableTexture();
            gettingResource = false;
        }

        if (!gettingResource) {
            Tessellator tessellator = Tessellator.getInstance();
            BufferBuilder bufferbuilder = tessellator.getBuffer();
            GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            GlStateManager.enableAlphaTest();
            GlStateManager.alphaFunc(GL11.GL_GREATER, 0.001f);
            GlStateManager.enableBlend();
            GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
            GlStateManager.shadeModel(GL11.GL_SMOOTH);
            RenderHelper.disableStandardItemLighting();
            bufferbuilder.begin(mode, DefaultVertexFormats.POSITION_TEX_COLOR);
            bufferbuilder.pos(x4, y4, z4).tex(tx4, ty4).color(rgba[3][0], rgba[3][1], rgba[3][2], (int) (rgba[3][3] * alpha)).endVertex();
            bufferbuilder.pos(x3, y3, z3).tex(tx3, ty3).color(rgba[2][0], rgba[2][1], rgba[2][2], (int) (rgba[2][3] * alpha)).endVertex();
            bufferbuilder.pos(x2, y2, z2).tex(tx2, ty2).color(rgba[1][0], rgba[1][1], rgba[1][2], (int) (rgba[1][3] * alpha)).endVertex();
            bufferbuilder.pos(x1, y1, z1).tex(tx1, ty1).color(rgba[0][0], rgba[0][1], rgba[0][2], (int) (rgba[0][3] * alpha)).endVertex();
            tessellator.draw();
        }
        RenderHelper.enableStandardItemLighting();
    }

    public static void draw_Res_Url_Xyz4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex4_Col4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex4_Col(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex2(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex2_Col4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex2_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex2_Col(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex2_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xyz4_Tex2_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Url_Xyz4_Col4(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Url_Xyz4_Col(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xyz4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xyz4_Mod(ResourceLocation resource, String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Url_Xy4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex4_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex4_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex2(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex2_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex2_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex2_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex2_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xy4_Tex2_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Url_Xy4_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Url_Xy4_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xy4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Url_Xy2(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex4_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex4_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex4_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex4_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex2(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex2_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex2_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex2_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex2_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xy2_Tex2_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Url_Xy2_Col4(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Col4_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Url_Xy2_Col(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Url_Xy2_Col_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Url_Xy2_Mod(ResourceLocation resource, String url, double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xyz4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Tex4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Tex4_Col4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Tex4_Col4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xyz4_Tex4_Col(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Tex4_Col_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xyz4_Tex4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xyz4_Tex2(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Tex2_Col4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Tex2_Col4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xyz4_Tex2_Col(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Tex2_Col_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xyz4_Tex2_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xyz4_Col4(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Col4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xyz4_Col(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xyz4_Col_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xyz4_Mod(ResourceLocation resource, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xy4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Tex4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Tex4_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Tex4_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xy4_Tex4_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Tex4_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xy4_Tex4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xy4_Tex2(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Tex2_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Tex2_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xy4_Tex2_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Tex2_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xy4_Tex2_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xy4_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xy4_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xy4_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xy4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xy2(ResourceLocation resource, double x1, double y1, double x2, double y2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Tex4(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Tex4_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Tex4_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xy2_Tex4_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Tex4_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xy2_Tex4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xy2_Tex2(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Tex2_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Tex2_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xy2_Tex2_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Tex2_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xy2_Tex2_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Res_Xy2_Col4(ResourceLocation resource, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Col4_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Res_Xy2_Col(ResourceLocation resource, double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Res_Xy2_Col_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Res_Xy2_Mod(ResourceLocation resource, double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(resource, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xyz4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Tex4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Tex4_Col4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Tex4_Col4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xyz4_Tex4_Col(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Tex4_Col_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xyz4_Tex4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xyz4_Tex2(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Tex2_Col4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Tex2_Col4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xyz4_Tex2_Col(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Tex2_Col_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xyz4_Tex2_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xyz4_Col4(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Col4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xyz4_Col(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xyz4_Col_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xyz4_Mod(String url, double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xy4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Tex4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Tex4_Col4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Tex4_Col4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xy4_Tex4_Col(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Tex4_Col_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xy4_Tex4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xy4_Tex2(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Tex2_Col4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Tex2_Col4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xy4_Tex2_Col(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Tex2_Col_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xy4_Tex2_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xy4_Col4(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Col4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xy4_Col(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xy4_Col_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xy4_Mod(String url, double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xy2(String url, double x1, double y1, double x2, double y2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Tex4(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Tex4_Col4(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Tex4_Col4_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xy2_Tex4_Col(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Tex4_Col_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xy2_Tex4_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xy2_Tex2(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Tex2_Col4(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Tex2_Col4_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xy2_Tex2_Col(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Tex2_Col_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xy2_Tex2_Mod(String url, double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Url_Xy2_Col4(String url, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Col4_Mod(String url, double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Url_Xy2_Col(String url, double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Url_Xy2_Col_Mod(String url, double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Url_Xy2_Mod(String url, double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, url, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xyz4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xyz4_Tex4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xyz4_Tex4_Col4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xyz4_Tex4_Col4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xyz4_Tex4_Col(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xyz4_Tex4_Col_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Xyz4_Tex4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xyz4_Tex2(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xyz4_Tex2_Col4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xyz4_Tex2_Col4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xyz4_Tex2_Col(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xyz4_Tex2_Col_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Xyz4_Tex2_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xyz4_Col4(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xyz4_Col4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xyz4_Col(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xyz4_Col_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Xyz4_Mod(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3, double x4, double y4, double z4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xy4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xy4_Tex4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xy4_Tex4_Col4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xy4_Tex4_Col4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xy4_Tex4_Col(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xy4_Tex4_Col_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Xy4_Tex4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xy4_Tex2(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xy4_Tex2_Col4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xy4_Tex2_Col4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xy4_Tex2_Col(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xy4_Tex2_Col_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Xy4_Tex2_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xy4_Col4(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xy4_Col4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xy4_Col(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xy4_Col_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Xy4_Mod(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y2, 0, x3, y3, 0, x4, y4, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xy2(double x1, double y1, double x2, double y2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xy2_Tex4(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xy2_Tex4_Col4(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xy2_Tex4_Col4_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xy2_Tex4_Col(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xy2_Tex4_Col_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, color, color, color, color, mode, alpha);
    }

    public static void draw_Xy2_Tex4_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double tx3, double ty3, double tx4, double ty4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty2, tx3, ty3, tx4, ty4, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xy2_Tex2(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], D_MOD, alpha);
    }

    public static void draw_Xy2_Tex2_Col4(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xy2_Tex2_Col4_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xy2_Tex2_Col(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xy2_Tex2_Col_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, color, color, color, color, mode, alpha);
    }

    public static void draw_Xy2_Tex2_Mod(double x1, double y1, double x2, double y2, double tx1, double ty1, double tx2, double ty2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, tx1, ty1, tx2, ty1, tx2, ty2, tx1, ty2, D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }

    public static void draw_Xy2_Col4(double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, D_MOD, alpha);
    }

    public static void draw_Xy2_Col4_Mod(double x1, double y1, double x2, double y2, int color1, int color2, int color3, int color4, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color1, color2, color3, color4, mode, alpha);
    }

    public static void draw_Xy2_Col(double x1, double y1, double x2, double y2, int color, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, D_MOD, alpha);
    }

    public static void draw_Xy2_Col_Mod(double x1, double y1, double x2, double y2, int color, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], color, color, color, color, mode, alpha);
    }

    public static void draw_Xy2_Mod(double x1, double y1, double x2, double y2, int mode, double alpha) {
        draw_Res_Url_Xyz4_Tex4_Col4_Mod(D_RES, D_URL, x1, y1, 0, x2, y1, 0, x2, y2, 0, x1, y2, 0, D_TEX[0], D_TEX[1], D_TEX[2], D_TEX[3], D_TEX[4], D_TEX[5], D_TEX[6], D_TEX[7], D_COL[0], D_COL[1], D_COL[2], D_COL[3], mode, alpha);
    }


}
