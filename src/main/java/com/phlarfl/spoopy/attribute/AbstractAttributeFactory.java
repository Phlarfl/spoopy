package com.phlarfl.spoopy.attribute;

import com.google.common.base.Strings;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractAttributeFactory {

    private transient final Map<String, Attribute> ATTRIBUTE_MAP = new HashMap<>();

    protected void setupAttributeFactory() {
        Field[] fields = getClass().getFields();
        for (Field field : fields)
            if (field.isAnnotationPresent(AttributeDescriptor.class))
                putAttribute(new Attribute(this, field));

        ATTRIBUTE_MAP.values().stream().filter((attribute) -> !Strings.isNullOrEmpty(attribute.getParentId())).forEach((attribute) -> attribute.setParent(ATTRIBUTE_MAP.get(attribute.getParentId())));
    }

    private void putAttribute(Attribute attribute) {
        ATTRIBUTE_MAP.put(attribute.getId(), attribute);
    }

    public Attribute getAttribute(String id) {
        if (ATTRIBUTE_MAP.containsKey(id))
            return ATTRIBUTE_MAP.get(id);
        return null;
    }

}
