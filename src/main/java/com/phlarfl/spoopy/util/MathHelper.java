package com.phlarfl.spoopy.util;

import net.minecraft.util.math.Vec3d;

import static java.lang.Math.*;

public class MathHelper {

    public static double wrap(double val, double max) {
        return (((val % max) + max) % max);
    }

    public static double angle(double x1, double y1, double x2, double y2) {
        return toDegrees(atan2(y2 - y1, x2 - x1));
    }

    public static double[] angle(double x1, double y1, double z1, double x2, double y2, double z2) {
        return new double[]{(toDegrees(atan2(z2 - z1, x2 - x1))) - 90, -(toDegrees(atan2(y2 - y1, hypot(x2 - x1, z2 - z1))))};
    }

    public static double distance(double x1, double y1, double x2, double y2) {
        return hypot(x2 - x1, y2 - y1);
    }

    public static double distance(double x1, double y1, double z1, double x2, double y2, double z2) {
        return new Vec3d(x1, y1, z1).distanceTo(new Vec3d(x2, y2, z2));
    }

}
