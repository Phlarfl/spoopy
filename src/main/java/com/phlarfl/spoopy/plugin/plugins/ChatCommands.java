package com.phlarfl.spoopy.plugin.plugins;

import com.phlarfl.spoopy.attribute.AttributeDescriptor;
import com.phlarfl.spoopy.command.CommandFactory;
import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import net.minecraftforge.client.event.ClientChatEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@PluginDescriptor(id = "chat_commands")
public class ChatCommands extends AbstractPlugin {

    @AttributeDescriptor(id = "prefix", property = "plugin.chat_commands.attribute.prefix")
    public String prefix = ".";

    public ChatCommands() {
        super();
        onEnable();
    }

    @SubscribeEvent
    public void onClientChatEvent(ClientChatEvent event) {
        if (enabled && event.getOriginalMessage().startsWith(prefix)) {
            event.setCanceled(true);
            if (mc.ingameGUI != null)
                mc.ingameGUI.getChatGUI().addToSentMessages(event.getOriginalMessage());
            CommandFactory.INSTANCE.execute(event.getOriginalMessage().substring(prefix.length()));
        }
    }

}
