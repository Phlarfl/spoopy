package com.phlarfl.spoopy.command;

import com.phlarfl.spoopy.command.exception.ArgumentCountException;
import com.phlarfl.spoopy.command.exception.CommandException;
import com.phlarfl.spoopy.command.exception.InvalidArgumentException;
import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.util.ChatHelper;
import com.phlarfl.spoopy.util.ClassHelper;
import com.phlarfl.spoopy.util.LogHelper;
import com.phlarfl.spoopy.util.MinecraftHelper;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public enum CommandFactory implements MinecraftHelper {
    INSTANCE;

    private final Map<Class<? extends Command>, Command> COMMAND_MAP = new HashMap<>();

    public void register() {
        new Reflections(ClassHelper.getPackageName(Command.class))
                .getSubTypesOf(Command.class)
                .stream().filter((cls) -> !ClassHelper.classOf(cls, AbstractPlugin.class))
                .forEach((cls) -> {
                    try {
                        Command command = cls.getConstructor().newInstance();
                        register(command);
                    } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
                        LogHelper.error(e);
                    }
                });
    }

    public void register(Command command) {
        COMMAND_MAP.put(command.getClass(), command);
    }

    public Collection<Command> getCommands() {
        return COMMAND_MAP.values();
    }

    public <T extends Command> T getCommand(Class<T> commandClass) {
        return commandClass.cast(COMMAND_MAP.get(commandClass));
    }

    public Command getCommand(String cmd) {
        for (Command command : COMMAND_MAP.values())
            if (command.getCommandId().equalsIgnoreCase(cmd) || command.getCommandAlias().equalsIgnoreCase(cmd))
                return command;
        return null;
    }

    public void execute(String message) {
        String[] args = message.split(" ");
        if (args.length == 0 || args[0].isEmpty())
            ChatHelper.print("commands.error.arguments.count.zero");
        else {
            Command command = getCommand(args[0]);
            if (command != null) {
                String[] argsWithoutCommand = new String[args.length - 1];
                System.arraycopy(args, 1, argsWithoutCommand, 0, argsWithoutCommand.length);
                try {
                    command.executeCommand(argsWithoutCommand);
                } catch (ArgumentCountException e) {
                    ChatHelper.print("commands.error.arguments.count", e.getMin(), e.getMax());
                    ChatHelper.print("commands.usage", command.getCommandUsage());
                } catch (InvalidArgumentException e) {
                    ChatHelper.print("commands.error.arguments.invalid", e.getArgument());
                    ChatHelper.print("commands.usage", command.getCommandUsage());
                } catch (CommandException e) {
                    ChatHelper.print("commands.error");
                    ChatHelper.print("commands.usage", command.getCommandUsage());
                }
            } else
                ChatHelper.print("commands.error.command.unknown", args[0]);
        }
    }

}
