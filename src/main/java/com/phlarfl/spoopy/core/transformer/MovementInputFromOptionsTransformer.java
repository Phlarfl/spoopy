package com.phlarfl.spoopy.core.transformer;

import com.phlarfl.spoopy.core.transformer.descriptor.ClassDescriptor;
import com.phlarfl.spoopy.core.transformer.descriptor.DescribedMethod;
import com.phlarfl.spoopy.core.transformer.descriptor.InsertType;
import cpw.mods.modlauncher.api.ITransformer;
import org.objectweb.asm.tree.ClassNode;

@ClassDescriptor("net.minecraft.util.MovementInputFromOptions")
public class MovementInputFromOptionsTransformer extends AbstractClassTransformer implements ITransformer<ClassNode> {

    @Override
    public void init() {
        methods.add(new DescribedMethod("tick", "(ZZ)V", InsertType.BEFORE, () -> {
            return "com.phlarfl.spoopy.eventbus.events.EventMovementInput event = new com.phlarfl.spoopy.eventbus.events.EventMovementInput($1, $2);" +
                    "com.phlarfl.spoopy.eventbus.EventFactory.INSTANCE.handleEvent(event);" +
                    "$1 = event.isSlow();" +
                    "$2 = event.isNoDampening();";
        }));
    }
}
