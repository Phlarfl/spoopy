package com.phlarfl.spoopy.plugin.plugins;

import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import net.minecraftforge.client.event.GuiScreenEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@PluginDescriptor(id = "clickgui")
public class ClickGui extends AbstractPlugin {

    public ClickGui() {
        super();
    }

    @SubscribeEvent
    public void onDrawScreenPostEvent(GuiScreenEvent.DrawScreenEvent.Post event) {

    }

}
