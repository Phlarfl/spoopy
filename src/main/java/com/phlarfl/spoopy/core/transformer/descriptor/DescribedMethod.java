package com.phlarfl.spoopy.core.transformer.descriptor;

import lombok.Getter;

import java.util.function.Supplier;

@Getter
public class DescribedMethod {

    private final String method, desc, code;
    private final InsertType insertType;
    private final int insertAt;

    public DescribedMethod(String method, String desc, InsertType insertType, Supplier<String> code) {
        this(method, desc, insertType, 0, code);
    }

    public DescribedMethod(String method, String desc, InsertType insertType, int insertAt, Supplier<String> code) {
        this.method = method;
        this.desc = desc;
        this.insertType = insertType;
        this.insertAt = insertAt;
        this.code = code.get();
    }

}
