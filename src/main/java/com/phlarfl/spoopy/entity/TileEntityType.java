package com.phlarfl.spoopy.entity;

import net.minecraft.tileentity.TileEntity;

public enum TileEntityType implements Type<TileEntity> {
    UNKNOWN(new Class[]{});

    private Class<? extends TileEntity>[] classes;

    TileEntityType(Class<? extends TileEntity>[] classes) {
        this.classes = classes;
    }

    @Override
    public Class<? extends TileEntity>[] getClasses() {
        return classes;
    }
}
