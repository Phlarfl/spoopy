package com.phlarfl.spoopy.plugin.plugins;

import com.mojang.blaze3d.platform.GlStateManager;
import com.phlarfl.spoopy.attribute.AttributeDescriptor;
import com.phlarfl.spoopy.attribute.type.ColorAttribute;
import com.phlarfl.spoopy.entity.EntityFactory;
import com.phlarfl.spoopy.plugin.AbstractPlugin;
import com.phlarfl.spoopy.plugin.PluginDescriptor;
import com.phlarfl.spoopy.render.RenderState;
import com.phlarfl.spoopy.render.geometry.Line;
import com.phlarfl.spoopy.util.ColorHelper;
import com.phlarfl.spoopy.util.MathHelper;
import com.phlarfl.spoopy.util.RenderHelper;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.HashMap;
import java.util.Map;

@PluginDescriptor(id = "tracer")
public class Tracer extends AbstractPlugin {

    //@formatter:off

    @AttributeDescriptor(id = "width", property = "plugin.tracer.attribute.width")
    public int width = 3;

    @AttributeDescriptor(id = "fade", property = "plugin.tracer.attribute.fade")
    public boolean fade = false;

        @AttributeDescriptor(parentId = "fade", id = "distance", property = "plugin.tracer.attribute.fade.distance")
        public int fadeDistance = 200;

    @AttributeDescriptor(id = "entities", property = "plugin.tracer.attribute.entities")
    public boolean entities = true;

        @AttributeDescriptor(parentId = "entities", id = "bosses", property = "plugin.tracer.attribute.entities.bosses")
        public boolean entitiesBosses = true;

            @AttributeDescriptor(parentId = "entities.bosses", id = "color", property = "plugin.tracer.attribute.entities.all.color")
            public ColorAttribute entitiesBossesColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "items", property = "plugin.tracer.attribute.entities.items")
        public boolean entitiesItems = true;

            @AttributeDescriptor(parentId = "entities.items", id = "color", property = "plugin.tracer.attribute.entities.all.color")
            public ColorAttribute entitiesItemsColor = new ColorAttribute(0xFFB080E0);

        @AttributeDescriptor(parentId = "entities", id = "merchants", property = "plugin.tracer.attribute.entities.merchants")
        public boolean entitiesMerchants = true;

            @AttributeDescriptor(parentId = "entities.merchants", id = "color", property = "plugin.tracer.attribute.entities.all.color")
            public ColorAttribute entitiesMerchantsColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "monsters", property = "plugin.tracer.attribute.entities.monsters")
        public boolean entitiesMonsters = true;

            @AttributeDescriptor(parentId = "entities.monsters", id = "color", property = "plugin.tracer.attribute.entities.all.color")
            public ColorAttribute entitiesMonstersColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "passives", property = "plugin.tracer.attribute.entities.passives")
        public boolean entitiesPassives = true;

            @AttributeDescriptor(parentId = "entities.passives", id = "color", property = "plugin.tracer.attribute.entities.all.color")
            public ColorAttribute entitiesPassivesColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "players", property = "plugin.tracer.attribute.entities.players")
        public boolean entitiesPlayers = true;

            @AttributeDescriptor(parentId = "entities.players", id = "color", property = "plugin.tracer.attribute.entities.all.color")
            public ColorAttribute entitiesPlayersColor = new ColorAttribute(0xFFFF0000);

        @AttributeDescriptor(parentId = "entities", id = "projectiles", property = "plugin.tracer.attribute.entities.projectiles")
        public boolean entitiesProjectiles = true;

            @AttributeDescriptor(parentId = "entities.players", id = "color", property = "plugin.tracer.attribute.entities.all.color")
            public ColorAttribute entitiesProjectilesColor = new ColorAttribute(0xFFFF8000);

    //@formatter:on

    private transient Map<Entity, Vec3d> positions = new HashMap<>();

    @SubscribeEvent
    public void onRenderWorldLastEvent(RenderWorldLastEvent event) {
        if (enabled)
            for (Entity entity : mc.world.getAllEntities())
                if (isRenderable(entity))
                    positions.put(entity, RenderHelper.worldToScreen(entity.getEyePosition(mc.getRenderPartialTicks())));
    }

    @SubscribeEvent
    public void onRenderGameOverlayEvent(RenderGameOverlayEvent.Pre event) {
        if (enabled && event.getType() == RenderGameOverlayEvent.ElementType.ALL) {

            double scx = mc.mainWindow.getWidth() / 2.;
            double scy = mc.mainWindow.getHeight() / 2.;

            GlStateManager.pushMatrix();
            double scale = mc.mainWindow.getGuiScaleFactor() / Math.pow(mc.mainWindow.getGuiScaleFactor(), 2);
            GlStateManager.scaled(scale, scale, scale);
            RenderState state = new RenderState(Line.DEFAULT_RENDER_STATES);
            state.store();

            for (Entity entity : positions.keySet()) {
                int color = getColor(entity);
                Vec3d pos = positions.get(entity);
                if (pos != null) {
                    double x = pos.getX();
                    double y = pos.getY();
                    boolean inverse = pos.getZ() > 1;
                    double angle = Math.toRadians(MathHelper.wrap(MathHelper.angle(scx, scy, pos.getX(), pos.getY()) + (inverse ? 180 : 0), 360));
                    double max = Math.hypot(scx, scy);
                    double distance = inverse ? max : Math.min(Math.hypot(scx - x, scy - y), max);

                    double fromX, fromY;

                    x = scx + (Math.cos(angle) * distance);
                    y = scy + (Math.sin(angle) * distance);

                    if (fade && distance > fadeDistance) {
                        fromX = scx + (Math.cos(angle) * fadeDistance);
                        fromY = scy + (Math.sin(angle) * fadeDistance);
                        Line.draw_Xy_Col2_Wid(scx, scy, fromX, fromY, ColorHelper.setAlpha(color, 0), color, width, 1);
                        Line.draw_Xy_Col_Wid(fromX, fromY, x, y, color, width, 1);
                    } else
                        Line.draw_Xy_Col2_Wid(scx, scy, x, y, fade ? ColorHelper.setAlpha(color, 0) : color, color, width, 1);
                }
            }

            state.restore();
            GlStateManager.popMatrix();

            positions.clear();
        }
    }

    private boolean isRenderable(Entity entity) {
        if (!entities) return false;
        switch (EntityFactory.INSTANCE.getType(entity)) {
            case BOSS:
                return entitiesBosses;
            case ITEM:
                return entitiesItems;
            case MERCHANT:
                return entitiesMerchants;
            case MONSTER:
                return entitiesMonsters;
            case PASSIVE:
                return entitiesPassives;
            case PLAYER:
                return entitiesPlayers;
            case PROJECTILE:
                return entitiesProjectiles;
            case UNKNOWN:
            default:
                return false;
        }
    }

    private int getColor(Entity entity) {
        switch (EntityFactory.INSTANCE.getType(entity)) {
            case BOSS:
                return entitiesBossesColor.getColor();
            case ITEM:
                return entitiesItemsColor.getColor();
            case MERCHANT:
                return entitiesMerchantsColor.getColor();
            case MONSTER:
                return entitiesMonstersColor.getColor();
            case PASSIVE:
                return entitiesPassivesColor.getColor();
            case PLAYER:
                return entitiesPlayersColor.getColor();
            case PROJECTILE:
                return entitiesProjectilesColor.getColor();
            case UNKNOWN:
            default:
                return 0;
        }
    }

}
