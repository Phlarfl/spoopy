package com.phlarfl.spoopy.core.transformer;

import com.phlarfl.spoopy.core.transformer.descriptor.ClassDescriptor;
import com.phlarfl.spoopy.core.transformer.descriptor.DescribedMethod;
import com.phlarfl.spoopy.core.transformer.descriptor.InsertType;
import cpw.mods.modlauncher.api.ITransformer;
import org.objectweb.asm.tree.ClassNode;

@ClassDescriptor("net.minecraft.client.renderer.entity.EntityRendererManager")
public class EntityRendererTransformer extends AbstractClassTransformer implements ITransformer<ClassNode> {

    @Override
    public void init() {
        methods.add(new DescribedMethod("renderEntity", "(Lnet/minecraft/entity/Entity;DDDFFZ)V", InsertType.AT, 6, () -> {
            return "com.phlarfl.spoopy.eventbus.EventFactory.INSTANCE.handleEvent(new com.phlarfl.spoopy.eventbus.events.EventRenderEntity($0, $1, $2, $3, $4, $5, $6));";
        }));
    }
}
