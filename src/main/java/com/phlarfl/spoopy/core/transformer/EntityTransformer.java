package com.phlarfl.spoopy.core.transformer;

import com.phlarfl.spoopy.core.transformer.descriptor.ClassDescriptor;
import com.phlarfl.spoopy.core.transformer.descriptor.DescribedMethod;
import com.phlarfl.spoopy.core.transformer.descriptor.InsertType;
import cpw.mods.modlauncher.api.ITransformer;
import org.objectweb.asm.tree.ClassNode;

@ClassDescriptor("net.minecraft.entity.Entity")
public class EntityTransformer extends AbstractClassTransformer implements ITransformer<ClassNode> {

    @Override
    public void init() {
        methods.add(new DescribedMethod("isPoseClear", "(Lnet/minecraft/entity/Pose;)Z", InsertType.BEFORE, () -> {
            return "com.phlarfl.spoopy.eventbus.events.EventEntityIsPoseClear event = new com.phlarfl.spoopy.eventbus.events.EventEntityIsPoseClear($0, $1, this.world.isCollisionBoxesEmpty(this, this.getBoundingBox($1)));" +
                    "com.phlarfl.spoopy.eventbus.EventFactory.INSTANCE.handleEvent(event);" +
                    "if (event.isCanceled()) return event.isPoseClear();";
        }));
    }
}
