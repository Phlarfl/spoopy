package com.phlarfl.spoopy.render;

import com.mojang.blaze3d.platform.GlStateManager;
import org.lwjgl.opengl.GL12;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static org.lwjgl.opengl.GL11.*;

public class RenderState {

    private static final Map<Integer, GlStateManager.FogMode> FOG_MODES = new HashMap<Integer, GlStateManager.FogMode>() {{
        for (GlStateManager.FogMode fogMode : GlStateManager.FogMode.values())
            put(fogMode.field_187351_d, fogMode);
    }};

    private static final Map<Integer, GlStateManager.CullFace> CULL_FACE_MODES = new HashMap<Integer, GlStateManager.CullFace>() {{
        for (GlStateManager.CullFace cullFaceMode : GlStateManager.CullFace.values())
            put(cullFaceMode.field_187328_d, cullFaceMode);
    }};

    public enum State {
        ALPHA((state) -> {
            state.alpha = glGetBoolean(GL_ALPHA_TEST);
            state.alphaFunc = glGetInteger(GL_ALPHA_TEST_FUNC);
            state.alphaRef = glGetFloat(GL_ALPHA_TEST_REF);
        }, (state) -> {
            if (state.alpha) GlStateManager.enableAlphaTest();
            else GlStateManager.disableAlphaTest();
            GlStateManager.alphaFunc(state.alphaFunc, state.alphaRef);
        }),

        LIGHTING((state) -> {
            state.lighting = glGetBoolean(GL_LIGHTING);
            for (int i = 0; i < state.light.length; i++)
                state.light[0] = glGetBoolean(GL_LIGHT0 + i);
        }, (state) -> {
            if (state.lighting) GlStateManager.enableLighting();
            else GlStateManager.disableLighting();
            for (int i = 0; i < state.light.length; i++)
                if (state.light[i]) GlStateManager.enableLight(i);
                else GlStateManager.disableLight(i);
        }),

        COLOR_MATERIAL((state) -> {
            state.colorMaterial = glGetBoolean(GL_COLOR_MATERIAL);
            state.colorMaterialFace = glGetInteger(GL_COLOR_MATERIAL_FACE);
            state.colorMaterialMode = glGetInteger(GL_COLOR_MATERIAL_PARAMETER);
        }, (state) -> {
            if (state.colorMaterial) GlStateManager.enableColorMaterial();
            else GlStateManager.disableColorMaterial();
            GlStateManager.colorMaterial(state.colorMaterialFace, state.colorMaterialMode);
        }),

        BLEND((state) -> {
            state.blend = glGetBoolean(GL_BLEND);
            state.blendSrc = glGetInteger(GL_BLEND_SRC);
            state.blendDst = glGetInteger(GL_BLEND_DST);
        }, (state) -> {
            if (state.blend) GlStateManager.enableBlend();
            else GlStateManager.disableBlend();
            GlStateManager.blendFunc(state.blendSrc, state.blendDst);
        }),

        DEPTH((state) -> {
            state.depth = glGetBoolean(GL_DEPTH_TEST);
            state.depthFunc = glGetInteger(GL_DEPTH_FUNC);
        }, (state) -> {
            if (state.depth) GlStateManager.enableDepthTest();
            else GlStateManager.disableDepthTest();
            GlStateManager.depthFunc(state.depthFunc);
            glDepthFunc(state.depthFunc);
        }),

        FOG((state) -> {
            state.fog = glGetBoolean(GL_FOG);
            state.fogMode = glGetInteger(GL_FOG_MODE);
            state.fogDensity = glGetFloat(GL_FOG_DENSITY);
            state.fogStart = glGetFloat(GL_FOG_START);
            state.fogEnd = glGetFloat(GL_FOG_END);
        }, (state) -> {
            if (state.fog) GlStateManager.enableFog();
            else GlStateManager.disableFog();
            GlStateManager.fogDensity(state.fogDensity);
            GlStateManager.fogMode(FOG_MODES.get(state.fogMode));
            GlStateManager.fogStart(state.fogStart);
            GlStateManager.fogEnd(state.fogEnd);
        }),

        CULL((state) -> {
            state.cull = glGetBoolean(GL_CULL_FACE);
            state.cullMode = glGetInteger(GL_CULL_FACE_MODE);
        }, (state) -> {
            if (state.cull) GlStateManager.enableCull();
            else GlStateManager.disableCull();
            GlStateManager.cullFace(CULL_FACE_MODES.get(state.cullMode));
        }),

        POLYGON((state) -> {
            state.polygonOffsetFill = glGetBoolean(GL_POLYGON_OFFSET_FILL);
            state.polygonOffsetLine = glGetBoolean(GL_POLYGON_OFFSET_LINE);
            state.polygonOffsetFactor = glGetFloat(GL_POLYGON_OFFSET_FACTOR);
            state.polygonOffsetUnits = glGetFloat(GL_POLYGON_OFFSET_UNITS);
        }, (state) -> {
            if (state.polygonOffsetFill) GlStateManager.enablePolygonOffset();
            else GlStateManager.disablePolygonOffset();
            if (state.polygonOffsetLine) GlStateManager.enableLineOffset();
            else GlStateManager.disableLineOffset();
            GlStateManager.polygonOffset(state.polygonOffsetFactor, state.polygonOffsetUnits);
        }),

        COLOR_LOGIC((state) -> {
            state.colorLogic = glGetBoolean(GL_COLOR_LOGIC_OP);
            state.colorLogicMode = glGetInteger(GL_LOGIC_OP_MODE);
        }, (state) -> {
            if (state.colorLogic) GlStateManager.enableColorLogicOp();
            else GlStateManager.disableColorLogicOp();
            GlStateManager.logicOp(state.colorLogicMode);
        }),

        TEX_GEN((state) -> {
            state.texGenQ = glGetBoolean(GL_TEXTURE_GEN_Q);
            state.texGenR = glGetBoolean(GL_TEXTURE_GEN_R);
            state.texGenS = glGetBoolean(GL_TEXTURE_GEN_S);
            state.texGenT = glGetBoolean(GL_TEXTURE_GEN_T);
            state.texGenQMode = glGetTexGeni(GL_Q, GL_TEXTURE_GEN_MODE);
            state.texGenRMode = glGetTexGeni(GL_R, GL_TEXTURE_GEN_MODE);
            state.texGenSMode = glGetTexGeni(GL_S, GL_TEXTURE_GEN_MODE);
            state.texGenTMode = glGetTexGeni(GL_T, GL_TEXTURE_GEN_MODE);
        }, (state) -> {
            if (state.texGenQ) GlStateManager.enableTexGen(GlStateManager.TexGen.Q);
            else GlStateManager.disableTexGen(GlStateManager.TexGen.Q);
            if (state.texGenR) GlStateManager.enableTexGen(GlStateManager.TexGen.R);
            else GlStateManager.disableTexGen(GlStateManager.TexGen.R);
            if (state.texGenS) GlStateManager.enableTexGen(GlStateManager.TexGen.S);
            else GlStateManager.disableTexGen(GlStateManager.TexGen.S);
            if (state.texGenT) GlStateManager.enableTexGen(GlStateManager.TexGen.T);
            else GlStateManager.disableTexGen(GlStateManager.TexGen.T);
            GlStateManager.texGenMode(GlStateManager.TexGen.Q, state.texGenQMode);
            GlStateManager.texGenMode(GlStateManager.TexGen.R, state.texGenRMode);
            GlStateManager.texGenMode(GlStateManager.TexGen.S, state.texGenSMode);
            GlStateManager.texGenMode(GlStateManager.TexGen.T, state.texGenTMode);
        }),

        STENCIL((state) -> {
            state.stencilFunc = glGetInteger(GL_STENCIL_FUNC);
            state.stencilRef = glGetInteger(GL_STENCIL_REF);
            state.stencilWriteMask = glGetInteger(GL_STENCIL_WRITEMASK);
            state.stencilValueMask = glGetInteger(GL_STENCIL_VALUE_MASK);
            state.stencilFail = glGetInteger(GL_STENCIL_FAIL);
            state.stencilPassDepthFail = glGetInteger(GL_STENCIL_PASS_DEPTH_FAIL);
            state.stencilPassDepthPass = glGetInteger(GL_STENCIL_PASS_DEPTH_PASS);
        }, (state) -> {
            GlStateManager.stencilFunc(state.stencilFunc, state.stencilRef, state.stencilWriteMask);
            GlStateManager.stencilMask(state.stencilValueMask);
            GlStateManager.stencilOp(state.stencilFail, state.stencilPassDepthFail, state.stencilPassDepthPass);
        }),

        NORMALIZE((state) -> {
            state.normalize = glGetBoolean(GL_NORMALIZE);
        }, (state) -> {
            if (state.normalize) GlStateManager.enableNormalize();
            else GlStateManager.disableNormalize();
        }),

        TEXTURE((state) -> {
            state.texture2d = glGetBoolean(GL_TEXTURE_2D);
        }, (state) -> {
            if (state.texture2d) GlStateManager.enableTexture();
            else GlStateManager.disableTexture();
        }),

        RESCALE_NORMAL((state) -> {
            state.rescaleNormal = GL12.glGetBoolean(GL12.GL_RESCALE_NORMAL);
        }, (state) -> {
            if (state.rescaleNormal) GlStateManager.enableRescaleNormal();
            else GlStateManager.disableRescaleNormal();
        }),

        SHADE_MODEL((state) -> {
            state.shadeModel = glGetInteger(GL_SHADE_MODEL);
        }, (state) -> {
            GlStateManager.shadeModel(state.shadeModel);
        }),

        LINE_WIDTH((state) -> {
            state.lineWidth = glGetFloat(GL_LINE_WIDTH);
        }, (state) -> {
            GlStateManager.lineWidth(state.lineWidth);
        });

        private final Consumer<RenderState> store, restore;

        State(Consumer<RenderState> store, Consumer<RenderState> restore) {
            this.store = store;
            this.restore = restore;
        }

        public void store(RenderState state) {
            store.accept(state);
        }

        public void restore(RenderState state) {
            restore.accept(state);
        }
    }

    private boolean alpha, lighting, colorMaterial, blend, depth, fog, cull, polygonOffsetFill, polygonOffsetLine, colorLogic, texGenQ, texGenR, texGenS, texGenT, normalize, texture2d, rescaleNormal;
    private int alphaFunc, colorMaterialFace, colorMaterialMode, blendSrc, blendDst, depthFunc, fogMode, cullMode, colorLogicMode, texGenQMode, texGenRMode, texGenSMode, texGenTMode, stencilFunc, stencilRef, stencilWriteMask, stencilValueMask, stencilFail, stencilPassDepthFail, stencilPassDepthPass, shadeModel;
    private float alphaRef, fogDensity, fogStart, fogEnd, polygonOffsetFactor, polygonOffsetUnits, lineWidth;
    private boolean[] light = new boolean[8];

    private State[] states;

    public RenderState(State... states) {
        this.states = states.length == 0 ? State.values() : states;
    }

    public void store() {
        for (State state : states)
            state.store(this);
    }

    public void restore() {
        for (State state : states)
            state.restore(this);
    }
}
